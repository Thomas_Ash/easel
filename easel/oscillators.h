//	oscillators.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Complex and modulation oscillators
// - bandlimited impulse train oscillator 
// - simple phase accumulator & sine oscillators
// - harmonic generation & integrating waveshapers
// - FM and AM

#ifndef __oscillators__
#define __oscillators__

#include "module.h"

#define KERNELSIZE 32			//points in sinc function kernel used to generate BLIT waveform
								//higher = less aliasing but more unwanted phase shift

#define LOOKUPSIZE 4096			//points in (all) look up tables

enum
{
    waveSpike = 0,
    waveSquare,
    waveTriangle,
    waveSawtooth
};

class LeakyIntegrator			//IIR filter; generates sawtooth, square & triangle from BLIT
{
    public:
        LeakyIntegrator(double in_b_1);
        ~LeakyIntegrator();

        double getSample(double x_0);

    protected:
        double b_1;
        double y_1;
};

class Lookup			//abstract lookup table class
{
    public:
        Lookup(int inSize);		//only allocates memory; derived class needs to populate k_0 and call interpolate
        ~Lookup();
        double getValue(double x);	//0 <= x < 1

    protected:
        void interpolate();		//perform linear interpolation; populates k_1

        int size;
        double* k_0;
        double* k_1;
};

class BlackmanWindow : public Lookup	//Blackman window function
{
    public:
        BlackmanWindow(int inSize);
        ~BlackmanWindow();
};

class SineLookup : public Lookup		//Sine lookup.. possibly not used any more
{
    public:
        SineLookup(int inSize);
        ~SineLookup();
};

class TimbreLookup : public Lookup	//Approximation of the Buchla harmonic-generating waveshaper transfer function
{
    public:
        TimbreLookup(int inSize);
        ~TimbreLookup();
};

class SincKernel		//windowed sinc kernel; fixed at 1/2 sampling rate for now
{
    public:
        SincKernel(double offset);
        ~SincKernel();

        static BlackmanWindow window;

        bool accSample(double* sample);

    protected:
        double x;
        double sinx;

};

class Oscillator	//basic oscillator class
{
    public:
        Oscillator(double inFreq, double inPhase);
        ~Oscillator();

        void setFreq(double inFreq);
        double getSample();		//simply returns phase accumulator; a DC-shifted, un-bandlimited sawtooth

    protected:
        double freq;
        double phase;
};

class BlitOsc : public Oscillator		//Band-Limited Impulse Train
{
    public:
        BlitOsc(double inFreq, double inPhase);
        ~BlitOsc();

        double getSample();

    protected:
        SincKernel* kernels [KERNELSIZE];
};

class SineOsc : public Oscillator		//sine wave
{
    public:
        SineOsc(double inFreq, double inPhase);
        ~SineOsc();

        double getSample();

};

class ComplexOsc : public Module
{
    public:
        ComplexOsc(double inPitch = 0.01, double inPitchFine = 0.0, double inPitchProc = 0.0,  int inPitchProcSign = 1,
			double inTimbre = 0.05, double inTimbreProc = 0.0, double inShape = 0.0, int inWave = waveSpike);
        ~ComplexOsc();

        void setPitch(double inPitch);
        void setPitchFine(double inPitchFine);
        void setPitchProc(double inPitchProc);
		void setPitchProcSign(int inPitchProcSign);
        void setTimbre(double inTimbre);
        void setTimbreProc(double inTimbreProc);
        void setShape(double inShape);
        void setWave(int inWave);

        Input* keyIn;   //inputs
        Input* expFModIn;
        Input* linFModIn;
        Input* timbreModIn;
        Input* ampModIn;

        Output* audioOut;    //output

        void process();

    protected:
        double pitch;       //VST params
		double pitchFine;
        double pitchProc;
		int pitchProcSign;
        double timbre;
        double timbreProc;
        double shape;
        int wave;

        BlitOsc* blit1;     //internal state
        BlitOsc* blit2;
        SineOsc* sine;

//        LeakyIntegrator* blit2saw;
        LeakyIntegrator* blit2square;
        LeakyIntegrator* square2tri;

        TimbreLookup* timbrefunc;

        double spike;
        double square;
        double tri;

        double audioSig;    //output signal
};

class ModulationOsc : public Module
{
	public:
		ModulationOsc(double inFreq = 0.001, double inFreqProc = 0.0, double inFreqFine = 0.0,
					double inIndex = 0.0, double inIndexProc = 0.0, int inWave = waveSawtooth);
		~ModulationOsc();

		void setFreq(double inFreq);
		void setFreqFine(double inFreqFine);
		void setFreqProc(double inFreqProc);
		void setIndex(double inIndex);
		void setIndexProc(double inIndexProc);
		void setWave(int inWave);

		Input* keyIn;		//inputs
		Input* fModIn;
		Input* indexModIn;

		Output* cvOut;		//outputs
		Output* fmOut;
		Output* amOut;

		void process();

	protected:
		double freq;		//VST params
		double freqFine;
		double freqProc;
		double index;
		double indexProc;
		int wave;

		BlitOsc* blit1;     //internal state
        BlitOsc* blit2;
		Oscillator* simple;

        LeakyIntegrator* blit2saw;
        LeakyIntegrator* blit2square;
        LeakyIntegrator* square2tri;

		double phase;
		double spike;
        double saw;
		double square;
        double tri;

		double cvSig;		//output signals
		double fmSig;
		double amSig;
};

#endif // __oscillators__
