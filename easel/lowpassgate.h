//	lowpassgate.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Lowpass Gate
// - Biquad lowpass audio filter
// - optoisolator slew limit model

#ifndef __lowpassgate__
#define __lowpassgate__

#include "module.h"

enum
{
    lpgModeFilter = 0,
    lpgModeCombo,
    lpgModeAmp,
};

class Vactrol	//1-pole IIR; switches to a faster time constant if input > output
{
    public:
        Vactrol();
        ~Vactrol();

        double getSample(double x_0);

    protected:
        double y_1;
        double b_1;
};

class Biquad	//textbook lowpass biquad IIR
{
    public:
        Biquad(double inGain, double inCutoff, double inQ);
        ~Biquad();

        void setGain(double inGain);
        void setCutoff(double inCutoff);
        void setQ(double inQ);

        void open();        //cutoff to 0.25

        double getSample(double x_0);

    protected:
        void update();

        double gain;
        double cutoff;
        double q;

        double a_0;
        double a_1;
        double a_2;
        double b_0;
        double b_1;
        double b_2;

        double x_1;
        double x_2;
        double y_1;
        double y_2;
};

class LowPassGate : public Module
{
    public:
        LowPassGate(double inOffset = 0.0, double inProcessing = 0.0, int inMode = lpgModeCombo);
        ~LowPassGate();

        void setOffset(double inOffset);			//VST params
        void setProcessing(double inProcessing);
        void setMode(int inMode);

        Input* audioIn;         //inputs
        Input* controlIn;

        Output* audioOut;        //outputs
        Output* ledOut;       

        void process();

    protected:
        double offset;          //VST params
        double processing;
        int mode;

        Biquad* filter;         //internal state
        Vactrol* opto;

        double audioSig;        //output signals
		double ledSig;
};

#endif // __lowpassgate__
