#include "Easel.h"
#include "IPlug_include_in_plug_src.h"
#include "IControl.h"
#include "resource.h"

const int totalPrograms = 1;

Easel::Easel(IPlugInstanceInfo instanceInfo) : IPLUG_CTOR(totalParams, totalPrograms, instanceInfo)
{
	TRACE;

	InitParams();

	InitGraphics();
	
	//MakePreset("preset 1", ... );
	MakeDefaultPreset((char *) "-", totalParams);
}

Easel::~Easel() {}

void Easel::ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames)
{
  // Mutex is already locked for us.

  double* in1 = inputs[0];
  double* in2 = inputs[1];
  double* out1 = outputs[0];
  double* out2 = outputs[1];

  for (int s = 0; s < nFrames; ++s, ++in1, ++in2, ++out1, ++out2)
  {
    *out1 = *in1 * mGain;
    *out2 = *in2 * mGain;
  }
}

void Easel::Reset()
{
	TRACE;
	IMutexLock lock(this);
}

void Easel::OnParamChange(int paramIdx)
{
	IMutexLock lock(this);

	int iValue = (int)GetParam(paramIdx)->Value();
	double dValue = GetParam(paramIdx)->Value();

	switch (paramIdx)
	{
		//random source
		case paramRanTrigSrc:
			switch(iValue)
			{
				case 0:
					_ran.trigIn->patch(_bri.pulseOut);
					break;
				case 1:
					_ran.trigIn->unpatch();
					break;
				case 2:
					_ran.trigIn->patch(_pul.pulseOut);
					break;			
			}
			break;

		//sequencer
		case paramSeqTrigSrc:
			switch(iValue)
			{
				case 0:
					_seq.trigIn->patch(_bri.pulseOut);
					break;
				case 1:
					_seq.trigIn->unpatch();
					break;
				case 2:
					_seq.trigIn->patch(_pul.pulseOut);
					break;			
			}
			break;
	
		case paramSeqStages:
			_seq.setStages(iValue);
			break;

		case paramSeqPul1:
		case paramSeqPul2:
		case paramSeqPul3:
		case paramSeqPul4:
		case paramSeqPul5:
			_seq.setPulse(iValue == 1, paramIdx - paramSeqPul1);
			break;

		case paramSeqLev1:
		case paramSeqLev2:
		case paramSeqLev3:
		case paramSeqLev4:
		case paramSeqLev5:
			_seq.setLevel(dValue, paramIdx - paramSeqLev1);
			break;
	
		//envelope
		case paramEnvTrigSrc:
			switch(iValue)
			{
				case 0:
					_env.trigIn->patch(_bri.pulseOut);
					break;
				case 1:
					_env.trigIn->patch(_pul.pulseOut);
					break;
				case 2:
					_env.trigIn->patch(_seq.pulseOut);
					break;			
			}
			break;

		case paramEnvMode:
			switch(iValue)
			{
				case 0:
					_env.setMode(envModeSustained);
					break;
				case 1:
					_env.setMode(envModeTransient);
					break;
				case 2:
					_env.setMode(envModeOff);
					break;			
			}
			break;

        case paramEnvAttack:
            _env.setAttack((long)(GetSampleRate() * dValue));
            break;

        case paramEnvDuration:
            _env.setDuration((long)(GetSampleRate() * dValue));
            break;

        case paramEnvDecay:
            _env.setDecay((long)(GetSampleRate() * dValue));
            break;

		//pulser
		case paramPulTrigSrc:
			switch(iValue)
			{
				case 0:
					_pul.trigIn->patch(_bri.pulseOut);
					break;
				case 1:
					_pul.trigIn->patch(_pul.pulseOut);
					break;
				case 2:
					_pul.trigIn->patch(_seq.pulseOut);
					break;			
			}
			break;

		case paramPulMode:
			switch(iValue)
			{
				case 0:
					_pul.setMode(pulModeTrig);
					break;
				case 1:
					_pul.setMode(pulModeOff);
					break;
				case 2:
					_pul.setMode(pulModeOne);
					break;			
			}
			break;

        case paramPulPeriod:
            _pul.setPeriod((long)(GetSampleRate() * dValue));
            break;

        case paramPulPeriodProc:
            _pul.setPeriodProc(dValue);
            break;

		//modulation oscillator
		case paramMOscDest:
			switch(iValue)
			{
				case 0:
					_pre.ampModIn->patch(_mOsc.amOut);
					_cOsc.ampModIn->unpatch();
					_cOsc.linFModIn->unpatch();
					break;
				case 1:
					_pre.ampModIn->unpatch();
					_cOsc.ampModIn->patch(_mOsc.amOut);
					_cOsc.linFModIn->unpatch();
					break;
				case 2:
					_pre.ampModIn->unpatch();
					_cOsc.ampModIn->unpatch();
					_cOsc.linFModIn->patch(_mOsc.fmOut);	
					break;			
			}
			break;

		case paramMOscKey:
			if(iValue == 1)
				_mOsc.keyIn->unpatch();
			else
				_mOsc.keyIn->patch(_bri.keyOut);
			break;

		case paramMOscFreq:
			_mOsc.setFreq(dValue);
			break;

		case paramMOscFreqProc:
			_mOsc.setFreqProc(dValue);
			break;

		case paramMOscIndex:
			_mOsc.setIndex(dValue);
			break;

		case paramMOscIndexProc:
			_mOsc.setIndexProc(dValue);
			break;

		case paramMOscWave:
			switch(iValue)
			{
				case 0:
					_mOsc.setWave(waveSawtooth);
					break;
				case 1:
					_mOsc.setWave(waveSquare);
					break;
				case 2:
					_mOsc.setWave(waveTriangle);
					break;			
			}
			break;

		//complex oscillator
		case paramCOscKey:
			if(iValue == 1)
				_cOsc.keyIn->unpatch();
			else
				_cOsc.keyIn->patch(_bri.keyOut);
			break;

		case paramCOscPitch:
			_cOsc.setPitch(dValue);
			break;

		case paramCOscPitchFine:
			_cOsc.setPitchFine(dValue);
			break;

		case paramCOscPitchProc:
			_cOsc.setPitchProc(dValue);
			break;

        case paramCOscPitchProcSign:
			if(iValue == 1)
				_cOsc.setPitchProcSign(1);
			else
				_cOsc.setPitchProcSign(-1);
			break;

        case paramCOscTimb:
			_cOsc.setTimbre(dValue);
			break;

        case paramCOscTimbProc:
			_cOsc.setTimbreProc(dValue);
			break;

        case paramCOscShape:
			_cOsc.setShape(dValue);
			break;

		case paramCOscWave:
			switch(iValue)
			{
				case 0:
					_cOsc.setWave(waveSpike);
					break;
				case 1:
					_cOsc.setWave(waveSquare);
					break;
				case 2:
					_cOsc.setWave(waveTriangle);
					break;			
			}
			break;

		//timbral gate 1
        case paramLpg1Offset:
            _lpg1.setOffset(dValue);
            break;

        case paramLpg1Proc:
            _lpg1.setProcessing(dValue);
            break;

		case paramLpg1Mode:
			switch(iValue)
			{
				case 0:
					_lpg1.setMode(lpgModeFilter);
					break;
				case 1:
					_lpg1.setMode(lpgModeCombo);
					break;
				case 2:
					_lpg1.setMode(lpgModeAmp);
					break;			
			}
			break;

		//timbral gate 2
		case paramLpg2AudioSrc:
			switch(iValue)
			{
				case 0:
					_lpg2.audioIn->patch(_pre.preampOut);
					break;
				case 1:
					_lpg2.audioIn->patch(_mOsc.cvOut);
					break;
				case 2:
					_lpg2.audioIn->patch(_lpg1.audioOut);
					break;			
			}
			break;	

        case paramLpg2Offset:
            _lpg2.setOffset(dValue);
            break;

        case paramLpg2Proc:
            _lpg2.setProcessing(dValue);
            break;

		case paramLpg2Mode:
			switch(iValue)
			{
				case 0:
					_lpg2.setMode(lpgModeFilter);
					break;
				case 1:
					_lpg2.setMode(lpgModeCombo);
					break;
				case 2:
					_lpg2.setMode(lpgModeAmp);
					break;			
			}
			break;

		//i/o panel
		case paramPortTime:
			_port.setTime((long)(GetSampleRate() * dValue));
			break;

		case paramKeyShift:
			switch(iValue)
			{
				case 0:
					_key.setShift(shiftOctave);
					break;
				case 1:
					_key.setShift(shiftNone);
					break;
				case 2:
					_key.setShift(shiftPreset);
					break;			
			}
			break;

		case paramKeyPreset1:
		case paramKeyPreset2:
		case paramKeyPreset3:
			_key.setPresetLevel(dValue, paramIdx - paramKeyPreset1);
			break;

		case paramPreampGain:
			switch(iValue)
			{
				case 0:
					_pre.setGain(preampGainLow);
					break;
				case 1:
					_pre.setGain(preampGainMid);
					break;
				case 2:
					_pre.setGain(preampGainHigh);
					break;			
			}
			break;

		case paramOutGainA:
			_out.setGainA(dValue);
			break;

		case paramOutGainB:
			_out.setGainB(dValue);
			break;

		case paramOutReverb:
	//		_rev.setWet(dValue);
	//		_rev.setDry(1.0-dValue);
			break;

		case paramOutMaster:
			_out.setGainMaster(dValue);
			break;

		case inBPulse:			//patch routings
		case inBPress:
		case inBKey:
		case inPulPeriod:
		case inMOscIndex:
		case inMOscFreq:
		case inCOscPitch:
		case inCOscTimb:
		case inLpg1Mod:
		case inLpg2Mod:
		case inInv:
		case inKPorta:
			if(iValue == -1)
				getInput(paramIdx)->unpatch();
			else
				getInput(paramIdx)->patch(getOutput(iValue));
			break;

	}




}

Input* Easel::getInput(int index)
{
	switch(index)
	{
		case inBPulse: return _bri.pulseIn; 
		case inBPress: return _bri.pressureIn;
		case inBKey: return _bri.keyIn;
		case inPulPeriod: return _pul.periodModIn;
		case inMOscIndex: return _mOsc.indexModIn;
		case inMOscFreq: return _mOsc.fModIn;
		case inCOscPitch: return _cOsc.expFModIn;
		case inCOscTimb: return _cOsc.timbreModIn;
		case inLpg1Mod: return _lpg1.controlIn;
		case inLpg2Mod: return _lpg2.controlIn;
		case inInv: return _inv.levelIn;
		case inKPorta: return _port.levelIn;
	}
	
	return NULL;
}

Output* Easel::getOutput(int index)
{
	switch(index)
	{
		case outRan1:
		case outRan2:
		case outRan3:
		case outRan4:
			return _ran.levelOut;

		case outSeq1:
		case outSeq2:
			return _seq.levelOut;

		case outEnv1:
		case outEnv2:
		case outEnv3:
			return _env.envOut;

		case outPul1:
		case outPul2:
		case outPul3:
			return _pul.rampOut;

		case outPress1:
		case outPress2:
		case outPress3:
		case outPress4:
			return _bri.pressureOut;

		case outInv:
			return _inv.levelOut;

		case outMOsc:
			return _mOsc.cvOut;

		case outEnvDet:
			return _pre.envDetOut;

		case outKPulse1:
		case outKPulse2:
			return _key.pulseOut;

		case outKPress1:
		case outKPress2:
			return _key.pressureOut;

		case outKPorta1:
		case outKPorta2:
			return _port.levelOut;

		case outKKey1:
		case outKKey2:
			return _key.keyOut;

		case outKPreset1:
		case outKPreset2:
			return _key.presetOut;
	}

	return NULL;
}

Output* Easel::getLedOutput(int index)
{
	switch(index)
	{
		case ledEnv:
			return _env.ledOut;
		case ledPul:
			return _pul.ledOut;
		case ledLpg1:
			return _lpg1.ledOut;
		case ledLpg2:
			return _lpg2.ledOut;
		case ledSeq1:
			return _seq.ledOut[0];
		case ledSeq2:
			return _seq.ledOut[1];
		case ledSeq3:
			return _seq.ledOut[2];
		case ledSeq4:
			return _seq.ledOut[3];
		case ledSeq5:
			return _seq.ledOut[4];
	}

	return NULL;
}

void Easel::InitParams()
{
	//arguments are: name, defaultVal, minVal, maxVal, step, label
	
	//random source
	GetParam(paramRanTrigSrc)->InitInt("RanTrigSrc", 0, 0, 2, "");

	//sequencer
	GetParam(paramSeqTrigSrc)->InitInt("SeqTrigSrc", 0, 0, 2, "");
	GetParam(paramSeqStages)->InitInt("SeqStages", 3, 3, 5, "");
	GetParam(paramSeqPul1)->InitInt("SeqPul1", 0, 0, 1, "");
	GetParam(paramSeqPul2)->InitInt("SeqPul2", 0, 0, 1, "");
	GetParam(paramSeqPul3)->InitInt("SeqPul3", 0, 0, 1, "");
	GetParam(paramSeqPul4)->InitInt("SeqPul4", 0, 0, 1, "");
	GetParam(paramSeqPul5)->InitInt("SeqPul5", 0, 0, 1, "");
	GetParam(paramSeqLev1)->InitDouble("SeqLev1", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramSeqLev2)->InitDouble("SeqLev2", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramSeqLev3)->InitDouble("SeqLev3", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramSeqLev4)->InitDouble("SeqLev4", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramSeqLev5)->InitDouble("SeqLev5", 0.0, 0.0, 1.0, 0.001, "");

	//envelope
	GetParam(paramEnvTrigSrc)->InitInt("EnvTrigSrc", 0, 0, 2, "");
	GetParam(paramEnvMode)->InitInt("EnvMode", 0, 0, 2, "");
	GetParam(paramEnvAttack)->InitDouble("EnvAttack", 0.001, 0.001, 10.0, 0.001, "");
	GetParam(paramEnvAttack)->SetShape(2.718);
	GetParam(paramEnvDuration)->InitDouble("EnvDuration", 0.001, 0.001, 10.0, 0.001, "");
	GetParam(paramEnvDuration)->SetShape(2.718);
	GetParam(paramEnvDecay)->InitDouble("EnvDecay", 0.001, 0.001, 10.0, 0.001, "");
	GetParam(paramEnvDecay)->SetShape(2.718);

	//pulser
	GetParam(paramPulTrigSrc)->InitInt("PulTrigSrc", 0, 0, 2, "");
	GetParam(paramPulMode)->InitInt("PulMode", 0, 0, 2, "");
	GetParam(paramPulPeriod)->InitDouble("PulPeriod", 0.001, 0.001, 10.0, 0.001, "");
	GetParam(paramPulPeriod)->SetShape(2.718);
	GetParam(paramPulPeriodProc)->InitDouble("PulPeriodProc", 0.0, 0.0, 3.0, 0.003, "");

	//modulation oscillator
	GetParam(paramMOscDest)->InitInt("MOscDest", 0, 0, 2, "");
	GetParam(paramMOscKey)->InitInt("MOscKey", 0, 0, 1, "");
	GetParam(paramMOscFreq)->InitDouble("MOscFreq", 0.0, 0.0, 8.0, 0.008, "");
	GetParam(paramMOscFreqFine)->InitDouble("MOscFreqFine", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramMOscFreqProc)->InitDouble("MOscFreqProc", 0.0, 0.0, 3.0, 0.003, "");
	GetParam(paramMOscIndex)->InitDouble("MOscIndex", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramMOscIndexProc)->InitDouble("MOscIndexProc", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramMOscWave)->InitInt("MOscWave", 0, 0, 2, "");

	//complex oscillator
	GetParam(paramCOscKey)->InitInt("COscKey", 0, 0, 1, "");
	GetParam(paramCOscPitch)->InitDouble("COscPitch", 0.0, 0.0, 5.0, 0.005, "");
	GetParam(paramCOscPitchFine)->InitDouble("COscPitchFine", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramCOscPitchProc)->InitDouble("COscPitchProc", 0.0, 0.0, 3.0, 0.003, "");
	GetParam(paramCOscPitchProcSign)->InitInt("COscPitchProcSign", 0, 0, 1, "");
	GetParam(paramCOscTimb)->InitDouble("COscTimb", 0.05, 0.05, 1.0, 0.001, "");
	GetParam(paramCOscTimbProc)->InitDouble("COscTimbProc", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramCOscShape)->InitDouble("COscShape", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramCOscWave)->InitInt("COscWave", 0, 0, 2, "");

	//timbral gate 1
	GetParam(paramLpg1Offset)->InitDouble("Lpg1Offset", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramLpg1Proc)->InitDouble("Lpg1Proc", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramLpg1Mode)->InitInt("Lpg1Mode", 0, 0, 2, "");

	//timbral gate 2
	GetParam(paramLpg2AudioSrc)->InitInt("Lpg2AudioSrc", 0, 0, 2, "");
	GetParam(paramLpg2Offset)->InitDouble("Lpg2Offset", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramLpg2Proc)->InitDouble("Lpg2Proc", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramLpg2Mode)->InitInt("Lpg2Mode", 0, 0, 2, "");

	//i/o panel
	GetParam(paramPortTime)->InitDouble("PortTime", 0.001, 0.001, 10.0, 0.001, "");
	GetParam(paramPulPeriod)->SetShape(2.718);
	GetParam(paramKeyShift)->InitInt("KeyShift", 0, 0, 2, "");
	GetParam(paramKeyPreset1)->InitDouble("KeyPreset1", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramKeyPreset2)->InitDouble("KeyPreset2", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramKeyPreset3)->InitDouble("KeyPreset3", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramPreampGain)->InitInt("PreampGain", 0, 0, 2, "");
	GetParam(paramOutGainA)->InitDouble("OutGainA", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramOutGainB)->InitDouble("OutGainB", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramOutReverb)->InitDouble("Reverb", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramOutMonitor)->InitDouble("Monitor", 0.0, 0.0, 1.0, 0.001, "");
	GetParam(paramOutMaster)->InitDouble("Master", 0.0, 0.0, 1.0, 0.001, "");

	//input patchpoints
	GetParam(inBPulse)->InitInt("inBPulse", 0, 0, totalOuts, "");
	GetParam(inBPress)->InitInt("inBPress", 0, 0, totalOuts, "");
	GetParam(inBKey)->InitInt("inBKey", 0, 0, totalOuts, "");
	GetParam(inPulPeriod)->InitInt("inPulPeriod", 0, 0, totalOuts, "");
	GetParam(inMOscIndex)->InitInt("inMOscIndex", 0, 0, totalOuts, "");
	GetParam(inMOscFreq)->InitInt("inMOscFreq", 0, 0, totalOuts, "");
	GetParam(inCOscPitch)->InitInt("inCOscPitch", 0, 0, totalOuts, "");
	GetParam(inCOscTimb)->InitInt("inCOscTimb", 0, 0, totalOuts, "");
	GetParam(inLpg1Mod)->InitInt("inLpg1Mod", 0, 0, totalOuts, "");
	GetParam(inLpg2Mod)->InitInt("inLpg2Mod", 0, 0, totalOuts, "");
	GetParam(inInv)->InitInt("inInv", 0, 0, totalOuts, "");
	GetParam(inKPorta)->InitInt("inKPorta", 0, 0, totalOuts, "");

}

void Easel::InitGraphics()
{
	IGraphics* pGraphics = MakeGraphics(this, kWidth, kHeight);
	pGraphics->AttachPanelBackground(&COLOR_RED);


	IBitmap knob = pGraphics->LoadIBitmap(KNOB_ID, KNOB_FN, kKnobFrames);

	pGraphics->AttachControl(new IKnobMultiControl(this, kGainX, kGainY, kGain, &knob));

	AttachGraphics(pGraphics);
}
