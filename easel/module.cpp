//	module.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Modular synth foundation classes
//Feedback handling and time sync

//in the other source files, you will see 'sub-module' classes with a double getSample(double x) style method
//these methods process for one sampling period _and_ return the sample, as they are only meant to be referred
//to by one thing (i.e. the module they are part of)

//true modules need to be capable of staying in sync in any patching configuration so the processing and
//sample retrieval have been seperated and formalized.

#include "module.h"
#include <cstdlib>

//Module
//======

int Module::count = 0;
Module** Module::all = NULL;


Module::Module()
{
    Module** temp = new Module* [count+1];

    for(int i=0; i<count; i++)
        temp[i] = all[i];

    temp[count++] = this;

    if(all)
        delete[] all;

    all = temp;

    ready = false;
}

Module::~Module()
{
    Module** temp = NULL;

    if(count>1)             //build new array
    {
        temp = new Module* [count-1];

        int j=0;
        for(int i=0; i<count; i++)
        {
            if(all[i] != this)
                temp[j++] = all[i];
        }

        delete[] all;
    }

    all = temp;
    count--;
}

void Module::update()
{
    if(!ready)
    {
        ready = true;       //not 'ready' yet
        process();          //but if this line ends up calling back to here it won't crash
    }
}

void Module::process()
{

}

void Module::tick()
{
    for(int i = 0; i<count; i++)
        all[i]->update();

    for(int i = 0; i<count; i++)
        all[i]->ready = false;
}

//Output
//======

Output::Output(Module* inModule, double* inSource)
{
    module = inModule;
    source = inSource;
}

void Output::patch(Input* inTarget)
{
    inTarget->patch(this);
}

double Output::getSample()
{
    module->update();
    return (*source);
}

double Output::getSampleNoUpdate()
{
    return (*source);
}


//Input
//=====

Input::Input()
{
    source = NULL;
}

void Input::patch(Output* inSource)
{
    source = inSource;
}

void Input::unpatch()
{
    source = NULL;
}

bool Input::isPatched()
{
    return (source!=NULL);
}

double Input::getSample()
{
    if(source)
        return source->getSample();

    return 0.0;
}

