#ifndef __EASEL__
#define __EASEL__

#include "IPlug_include_in_plug_hdr.h"

#include "module.h"
#include "cv.h"
#include "lowpassgate.h"
#include "oscillators.h"
#include "io.h"
//#include "reverbmodel.h"

enum EParams
{
	paramRanTrigSrc = 0,
	paramSeqTrigSrc, paramSeqStages, paramSeqPul1, paramSeqPul2, paramSeqPul3, paramSeqPul4,
	paramSeqPul5, paramSeqLev1, paramSeqLev2, paramSeqLev3, paramSeqLev4, paramSeqLev5,
	paramEnvTrigSrc, paramEnvMode, paramEnvAttack, paramEnvDuration, paramEnvDecay,
	paramPulTrigSrc, paramPulMode, paramPulPeriod, paramPulPeriodProc,
	paramMOscDest, paramMOscKey, paramMOscFreq,	paramMOscFreqFine, paramMOscFreqProc, 
	paramMOscIndex, paramMOscIndexProc,	paramMOscWave,
	paramCOscKey, paramCOscPitch, paramCOscPitchFine, paramCOscPitchProc, paramCOscPitchProcSign,
	paramCOscTimb, paramCOscTimbProc, paramCOscShape, paramCOscWave,
	paramLpg1Offset, paramLpg1Proc, paramLpg1Mode,
	paramLpg2AudioSrc, paramLpg2Offset, paramLpg2Proc, paramLpg2Mode,
	paramPortTime, 
	paramKeyShift, paramKeyPreset1, paramKeyPreset2, paramKeyPreset3, 
	paramPreampGain,
	paramOutGainA, paramOutGainB, paramOutReverb, paramOutMonitor, paramOutMaster,

	inBPulse, inBPress, inBKey, inPulPeriod, inMOscIndex, inMOscFreq, inCOscPitch,
	inCOscTimb,	inLpg1Mod, inLpg2Mod, inInv, inKPorta,

	totalParams
};

enum EPatchOuts
{
	outRan1 = 0, outRan2, outRan3, outRan4, outSeq1, outSeq2, outEnv1, outEnv2, outEnv3,
	outPul1, outPul2, outPul3, outPress1, outPress2, outPress3, outPress4, outInv,
	outMOsc, outEnvDet,	outKPulse1,	outKPulse2,	outKPress1,	outKPress2,	outKPorta1,
	outKPorta2,	outKKey1, outKKey2,	outKPreset1, outKPreset2,
	totalOuts
};

enum ELEDs
{
	ledEnv = 0, ledPul, ledLpg1, ledLpg2, ledSeq1, ledSeq2, ledSeq3, ledSeq4, ledSeq5,
	totalLEDs
};

enum ELayout
{
  kWidth = GUI_WIDTH,
  kHeight = GUI_HEIGHT,

  kGainX = 100,
  kGainY = 100,
  kKnobFrames = 60
};

class Easel : public IPlug
{
	public:
		Easel(IPlugInstanceInfo instanceInfo);
		~Easel();

		void Reset();
		void OnParamChange(int paramIdx);
		void ProcessDoubleReplacing(double** inputs, double** outputs, int nFrames);

		Input* getInput(int index);
		Output* getOutput(int index);
		Output* getLedOutput(int index);

	private:

        Random _ran;			//modules
		Sequencer _seq;
        EnvGen _env;
		Pulser _pul;
		ModulationOsc _mOsc;
        ComplexOsc _cOsc;
        LowPassGate _lpg1;
        LowPassGate _lpg2;
		Inverter _inv;
		Portamento _port;
		Bridge _bri;
		Keyboard _key;
		PreampDet _pre;
		OutputMix _out;
	//	ReverbModel _rev;

		void InitParams();
		void InitGraphics();







};

#endif
