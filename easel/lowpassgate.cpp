//	lowpassgate.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Lowpass Gate
// - Biquad lowpass audio filter
// - optoisolator slew limit model

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <cmath>

#include "module.h"

#include "lowpassgate.h"

//Vactrol
//=======

Vactrol::Vactrol()
{
    y_1 = 0.0;
    b_1 = 0.995;
}

Vactrol::~Vactrol()
{

}

double Vactrol::getSample(double x_0)
{
    double y_0;

    x_0 = (x_0 < 0.0) ? 0.0 : x_0;
    x_0 = (x_0 > .995) ? (.995 + (x_0-.995) / 5) : x_0;
    x_0 = (x_0 > 1.0) ? 1.0 : x_0;

    b_1 = (x_0 > y_1) ? 0.995 : 0.9999;

    y_0 = x_0*(1-b_1) + y_1*b_1;

    y_1 = y_0;

    return y_0;
}

//Biquad
//======

Biquad::Biquad(double inGain, double inCutoff, double inQ)
{
    setGain(inGain);
    setCutoff(inCutoff);
    setQ(inQ);

    x_1 = x_2 = y_1 = y_2 = 0.0;
}

Biquad::~Biquad()
{

}

void Biquad::setGain(double inGain)
{
    if(inGain < 0.0)
        gain = 0.0;
    else if(inGain > 1.0)
        gain = 1.0;
    else
        gain = inGain;

    update();
}

void Biquad::setCutoff(double inCutoff)
{
    if(inCutoff < 0.0001)
        cutoff = 0.0001;
    else if(inCutoff > 0.49)
        cutoff = 0.49;
    else
        cutoff = inCutoff;

    update();
}

void Biquad::setQ(double inQ)
{
    if(inQ < 0.01)
        q = 0.01;
    else if(inQ > 3.0)
        q = 3.0;
    else
        q = inQ;

    update();
}

void Biquad::open()
{
    cutoff = 0.25;

    double alpha = 1/(2*q);

    a_0 = 0.5;
    a_1 = 1.0;
    a_2 = 0.5;

    b_0 = 1 + alpha;
    b_1 = 0.0;
    b_2 = 1 - alpha;
}

double Biquad::getSample(double x_0)
{
    double y_0;

    y_0 = (a_0*x_0 + a_1*x_1 + a_2*x_2 - b_1*y_1 - b_2*y_2) / b_0;

    x_2 = x_1;
    x_1 = x_0;
    y_2 = y_1;
    y_1 = y_0;

    return gain*y_0;
}

void Biquad::update()
{
    double w0 = 2*M_PI*cutoff;
    double cos_w0 = cos(w0);
    double alpha = sin(w0)/(2*q);

    a_0 = (1 - cos_w0)/2;
    a_1 = 1 - cos_w0;
    a_2 = (1 - cos_w0)/2;

    b_0 = 1 + alpha;
    b_1 = -2.0*cos_w0;
    b_2 = 1 - alpha;
}

//LowPassGate
//===========

LowPassGate::LowPassGate(double inOffset, double inProcessing, int inMode) : Module()
{
    setOffset(inOffset);
    setProcessing(inProcessing);
    setMode(inMode);

    filter = new Biquad(0.0, 0.5, 1.0);
    opto = new Vactrol();

	audioSig = ledSig = 0.0;

    audioIn = new Input();
    controlIn = new Input();

    audioOut = new Output(this, &audioSig);
	ledOut = new Output(this, &ledSig);
}

LowPassGate::~LowPassGate()
{
    delete filter;
    delete opto;

	delete audioIn;
	delete controlIn;

	delete audioOut;
	delete ledOut;
}

void LowPassGate::setOffset(double inOffset)
{
    if(inOffset < 0.0)
        offset = 0.0;
    else if(inOffset > 1.0)
        offset = 1.0;
    else
        offset = inOffset;

}

void LowPassGate::setProcessing(double inProcessing)
{
    if(inProcessing < 0.0)
        processing = 0.0;
    else if(inProcessing > 1.0)
        processing = 1.0;
    else
        processing = inProcessing;
}

void LowPassGate::setMode(int inMode)
{
    if(inMode==lpgModeFilter)
        mode = lpgModeFilter;
    else if(inMode==lpgModeAmp)
        mode = lpgModeAmp;
    else
        mode = lpgModeCombo;
}

void LowPassGate::process()
{
    ledSig = opto->getSample(offset + processing*controlIn->getSample());

    if(mode==lpgModeAmp)
        filter->open();
    else
        filter->setCutoff(pow(ledSig, 4) / 4);

    if(mode==lpgModeFilter)
        filter->setGain(1.0);
    else
        filter->setGain(pow(ledSig, 4));

    audioSig = filter->getSample(audioIn->getSample());
}
