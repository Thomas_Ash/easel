//	reverbmodel.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//	FFT convolution reverb using embedded impulse


#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif

#include <cmath>
#include <fftw3.h>
#include <windows.h>

#include "reverbmodel.h"

#define MODULENAME "easel.dll"	//name of this module, in which impulse.wav is embedded

ReverbModel::ReverbModel(double inWet, double inDry)
{
	ioSize = fftSize = -1;			//force allocation on first process call
	impFreq = audioFreq = NULL;
	fftTime = overlap = NULL;

	setWet(inWet);
	setDry(inDry);

	loadImpulse();
}

ReverbModel::~ReverbModel()
{
	deallocate();
}

void ReverbModel::setWet(double inWet)
{
	if(inWet < 0.0)
		wet = 0.0;
	else if(inWet > 1.0)
		wet = 1.0;
	else
		wet = inWet;
}

void ReverbModel::setDry(double inDry)
{
	if(inDry < 0.0)
		dry = 0.0;
	else if(inDry > 1.0)
		dry = 1.0;
	else
		dry = inDry;
}

void ReverbModel::process(double* in, double* out, long size)
{
	setIoSize(size);		//fft arrays are now allocated if they weren't before
	
	long i;

	for(i=0; i<fftSize; i++)	//load audio into zero padded array for fft
	{
		fftTime[i] = (i < ioSize) ? in[i] : 0.0;	
	}

	fftw_execute(forwardPlan);		//forward FFT

	double tempRe;

	for(i=0; i< fftSize; i++)		//convolution as complex multiplication
	{
		tempRe = audioFreq[i][0] * impFreq[i][0] - audioFreq[i][1] * impFreq[i][1];
		audioFreq[i][1] = audioFreq[i][0] * impFreq[i][1] + audioFreq[i][1] * impFreq[i][0];
		audioFreq[i][0] = tempRe;
	}

	fftw_execute(inversePlan);		//forward FFT

	double temp;
	double scale = 1.0/fftSize;

	for(i=0; i< fftSize; i++)		//get audio from complex array
	{
		temp = fftTime[i] * scale;

		if(i < (fftSize - ioSize))
			temp += overlap[i];

		if(i < ioSize)
			out[i] = wet*temp + dry*in[i];
		else
			overlap[i-ioSize] = temp;
	}

}

void ReverbModel::loadImpulse()
{
	//open resource
	HMODULE module;
	HRSRC resFind;		
	HGLOBAL resLoad;
	long resSize;
	BYTE* resLock;

	module = GetModuleHandle(MODULENAME);
	resFind = FindResource(module, "ID_IMPULSE", "DATA");
	resLoad = LoadResource(module, resFind);
	resSize = SizeofResource(module, resFind);
	resLock = (BYTE*)LockResource(resLoad);

	//read (as little as possible of) header
	int numChannels;
	int bytesPerSample;

	numChannels = *((WORD*)(resLock + 22));
	bytesPerSample = *((WORD*)(resLock + 34)) / 8;

	impSize = *((DWORD*)(resLock + 40)) / (numChannels*bytesPerSample) / 2;

	//read audio data
	impTime = new double[impSize];

	int step = numChannels*bytesPerSample;

	unsigned char temp8;
	signed short int temp16;
	signed long int temp32;

	resLock += 44;

	switch(bytesPerSample)
	{
		case 1:		//8 bit
			for(long i=0; i<impSize; i++)
			{
				temp8 = *(resLock += step);
				impTime[i] = (double)temp8 / 127 - 1.0;
			}
			break;

		case 2:		//16 bit
			for(long i=0; i<impSize; i++)
			{
				temp16 = *((WORD*)(resLock += step));
				impTime[i] = (double)temp16 / 32768;
			}
			break;		
		
		case 4:		//32 bit
			for(long i=0; i<impSize; i++)
			{
				temp32 = *((DWORD*)(resLock += step));
				impTime[i] = (double)temp32 / 2147483648;
			}
			break;
	}	
}

void ReverbModel::setIoSize(long inIoSize)
{
	if(ioSize == inIoSize)
		return;			//nothing to do here

	ioSize = inIoSize;

	fftSize = (long)pow(2, ceil(log((double)ioSize+impSize)/M_LN2));

	deallocate();

	fftTime = new double[fftSize];	
	impFreq = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * fftSize);	
	audioFreq = (fftw_complex*)fftw_malloc(sizeof(fftw_complex) * fftSize);	
	overlap = new double[fftSize-ioSize];

	long i;

	for(i=0; i<fftSize; i++)
	{
		fftTime[i] = (i < impSize) ? impTime[i] : 0.0;	//load impulse into zero padded array for fft
		if(i<(fftSize-ioSize))
			overlap[i] = 0.0;					
	}

    fftw_plan impPlan = fftw_plan_dft_r2c_1d(fftSize, fftTime, impFreq, FFTW_ESTIMATE);

    fftw_execute(impPlan);		//forward FFT

    fftw_destroy_plan(impPlan);	

	forwardPlan = fftw_plan_dft_r2c_1d(fftSize, fftTime, audioFreq, FFTW_ESTIMATE);
	inversePlan = fftw_plan_dft_c2r_1d(fftSize, audioFreq, fftTime, FFTW_ESTIMATE);
}

void ReverbModel::deallocate()
{
	if(fftTime)
		delete[] fftTime;

	if(impFreq)
		fftw_free(impFreq);

	if(audioFreq)
		fftw_free(audioFreq);

	if(overlap)
		delete[] overlap;

	fftw_cleanup();
}

