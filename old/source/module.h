//	module.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Modular synth foundation classes
//Feedback handling and time sync

//in the other source files, you will see 'sub-module' classes with a double getSample(double x) style method
//these methods process for one sampling period _and_ return the sample, as they are only meant to be referred
//to by one thing (i.e. the module they are part of)

//true modules need to be capable of staying in sync in any patching configuration so the processing and
//sample retrieval have been seperated and formalized.


#ifndef __module__
#define __module__

class Module	//base class of all modules
{
    public:
        Module();				//maintains count and all
        virtual ~Module();		//...as does this

        void update();				//processes if not ready
        virtual void process();		//does nothing; derived classes need to override

        static void tick();		//processes any instance that's not ready, then un-readys all instances

    private:
        static int count;		//number of instances
        static Module** all;	//pointers to all instances

        bool ready;		//has process() been called this tick?
};

class Input;

class Output
{
    public:
        Output(Module* inModule, double* inSource);
		~Output() {}

        void patch(Input* target);	//tells target to patch this

        double getSample();		//called by a patched input or the VST plugin class itself
							//updates module then pulls sample from source

		double getSampleNoUpdate();		//GUI can call this from outside the processing thread 
										//without screwing things up

    protected:
        Module* module;
        double* source;

};

class Input
{
    public:
        Input();
		~Input() {}

        void patch(Output* inSource);
        void unpatch();

        bool isPatched();

        double getSample();		//called by the instantiating module during process()
							//returns source->getSample(), or zero if unpatched

    protected:
        Output* source;
};

#endif // __module__
