//	easel.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Main class for VST Music Easel

#include <cmath>
#include <cstdlib>

#include <audioeffectx.h>

#include "module.h"
#include "cv.h"
#include "lowpassgate.h"
#include "io.h"
//#include "reverbmodel.h"

#include "easel.h"
#include "easeleditor.h"

AudioEffect* createEffectInstance (audioMasterCallback audioMaster)
{
    return new Easel (audioMaster);
}

//EaselProgram
//============

EaselProgram::EaselProgram(int index)
{
	for(int i=0; i<totalParams; i++)
	{
		param[i] = 0.0;
	}
	vst_strncpy (name, "", kVstMaxProgNameLen);
}

//Easel
//==========

Easel::Easel(audioMasterCallback audioMaster)
: AudioEffectX(audioMaster, NUMPROGRAMS, totalParams)
{
    setNumInputs (1);		// mono in
	setNumOutputs (1);		// mono out
	setUniqueID ('McEl');	// identify
	canProcessReplacing ();	// supports replacing output
	canDoubleReplacing ();	// supports replacing output
	isSynth(false);			//seems to work better in Reaper at least

    ran = new Random();				//modules
	seq = new Sequencer();
    env = new EnvGen();
    pul = new Pulser();
	mOsc = new ModulationOsc();
	cOsc = new ComplexOsc();
	lpg1 = new LowPassGate();
	lpg2 = new LowPassGate();
	inv = new Inverter();
	port = new Portamento();
	bri = new Bridge();
	key = new Keyboard();
	pre = new PreampDet();
	out = new OutputMix();
	//rev = new ReverbModel();

	char temp[kVstMaxProgNameLen+1];
	int i;			

	for(i=0; i<NUMPROGRAMS; i++)
	{
		program[i] = new EaselProgram(i);

		vst_strncpy(program[i]->name, "Default ", kVstMaxProgNameLen);
		int2string(i+1, temp,  kVstMaxProgNameLen);
		vst_strncat(program[i]->name, temp, kVstMaxProgNameLen);
	}

	currentProg = 0;

	for(i=0; i<totalParams; i++)
	{
		setParameter(i, 0.0);
	}

	(lpg1->audioIn)->patch(cOsc->audioOut);		//hardwiring
	(out->audioAIn)->patch(lpg1->audioOut);
	(out->audioBIn)->patch(lpg2->audioOut);

	//GUI

    setEditor (new EaselEditor (this));		

	holdingInput = holdingOutput = -1;
}

Easel::~Easel()
{
	for(int i=0; i<NUMPROGRAMS; i++)
	{
		delete program[i];
	}

    delete ran;		//modules
	delete seq;
    delete env;
	delete pul;
    delete mOsc;
    delete cOsc;
    delete lpg1;
    delete lpg2;
	delete inv;
	delete port;
	delete bri;
	delete key;
	delete pre;
	delete out;
	//delete rev;
}

void Easel::processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames)
{
    float* outBuf = outputs[0];
	pre->setBuffer(NULL, 0);		

    for(VstInt32 i=0; i<sampleFrames; i++)
    {
        outBuf[i] = (float)((out->audioOut)->getSample());
        Module::tick();
    }
}

void Easel::processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames)
{
	double* outBuf = outputs[0];
	double* inBuf = inputs[0];
	pre->setBuffer(inBuf, (long)sampleFrames);

    for(VstInt32 i=0; i<sampleFrames; i++)
    {
        outBuf[i] = (out->audioOut)->getSample();
        Module::tick();
    }

	//rev->process(outBuf, outBuf, sampleFrames);

}

void Easel::setParameter (VstInt32 index, float value)
{
    param[index] = value;
	program[currentProg]->param[index] = value;

    switch(index)
    {
		//RANDOM
		case paramRanTrigSrc:
            if(value<0.33)
				(ran->trigIn)->patch(bri->pulseOut);	//KEYBOARD TRIG OUT
            else if(value>0.66)
				(ran->trigIn)->unpatch();		
            else
				(ran->trigIn)->patch(pul->pulseOut);
            break;

		//SEQUENCER
		case paramSeqTrigSrc:
            if(value<0.33)
				(seq->trigIn)->patch(bri->pulseOut);		//KEYBOARD TRIG OUT
            else if(value>0.66)
				(seq->trigIn)->unpatch();		
            else
				(seq->trigIn)->patch(pul->pulseOut);
            break;


		case paramSeqStages:
            if(value<0.33)
				seq->setStages(3);
            else if(value>0.66)
				seq->setStages(5);
            else
				seq->setStages(4);
            break;
			
		case paramSeqPul1:
		case paramSeqPul2:
		case paramSeqPul3:
		case paramSeqPul4:
		case paramSeqPul5:
			seq->setPulse((value < 0.5), index-paramSeqPul1);
			break;

		case paramSeqLev1:
		case paramSeqLev2:
		case paramSeqLev3:
		case paramSeqLev4:
		case paramSeqLev5:
			seq->setLevel(value, index-paramSeqLev1);
			break;

		//ENVELOPE GENERATOR
		case paramEnvTrigSrc:
            if(value<0.33)	
				(env->trigIn)->patch(bri->pulseOut);		//KEYBOARD TRIG OUT
            else if(value>0.66)
				(env->trigIn)->patch(seq->pulseOut);		//SEQUENCER TRIG OUT
            else
				(env->trigIn)->patch(pul->pulseOut);
            break;			

		case paramEnvMode:
            if(value<0.33)
				env->setMode(envModeSustained);
            else if(value>0.66)
				env->setMode(envModeOff);
            else
				env->setMode(envModeTransient);
            break;		

        case paramEnvAttack:
            env->setAttack((long)(sampleRate*exp(2.3-8.52*value)));
            break;

        case paramEnvDuration:
            env->setDuration((long)(sampleRate*exp(2.3-8.52*value)));
            break;

        case paramEnvDecay:
            env->setDecay((long)(sampleRate*exp(2.3-8.52*value)));
            break;		

		//PULSER

		case paramPulTrigSrc:
            if(value<0.33)
				pul->trigIn->patch(bri->pulseOut);		//KEYBOARD TRIG OUT	
            else if(value>0.66)
				pul->trigIn->patch(seq->pulseOut);	//SEQUENCER TRIG OUT
				
            else  
				pul->trigIn->patch(pul->pulseOut);
            break;	

		case paramPulMode:			
			if(value<0.33)
				pul->setMode(pulModeTrig);
            else if(value>0.66)
				pul->setMode(pulModeOne);
            else
				pul->setMode(pulModeOff);
            break;		

        case paramPulPeriod:
            pul->setPeriod((long)(sampleRate*exp(2.3-8.52*value)));
            break;

        case paramPulPeriodProc:
            pul->setPeriodProc((double)value*3.0);
            break;

		//MODULATION OSCILLATOR

		case paramMOscDest:
            if(value<0.33)		//am ext
			{
				(pre->ampModIn)->patch(mOsc->amOut);
				(cOsc->ampModIn)->unpatch();
				(cOsc->linFModIn)->unpatch();
			}			
            else if(value>0.66)				//fm osc
			{
				(pre->ampModIn)->unpatch();
				(cOsc->ampModIn)->unpatch();
				(cOsc->linFModIn)->patch(mOsc->fmOut);	
			}
            else							//am osc
			{
				(pre->ampModIn)->unpatch();
				(cOsc->ampModIn)->patch(mOsc->amOut);
				(cOsc->linFModIn)->unpatch();
			}
            break;	

		case paramMOscKey:
			if(value<0.5)
				(mOsc->keyIn)->patch(bri->keyOut);
			else
				(mOsc->keyIn)->unpatch();
			break;

		case paramMOscFreq:
			mOsc->setFreq((double)8.0*value);
			break;

		case paramMOscFreqFine:
			mOsc->setFreqFine((double)value);
			break;

		case paramMOscFreqProc:
			mOsc->setFreqProc((double)3.0*value);
			break;

		case paramMOscIndex:
			mOsc->setIndex((double)value);
			break;

		case paramMOscIndexProc:
			mOsc->setIndexProc((double)value);
			break;

		case paramMOscWave:
			if(value<0.33)
                mOsc->setWave(waveSawtooth);
            else if(value>0.66)
                mOsc->setWave(waveTriangle);
            else
                mOsc->setWave(waveSquare);
            break;

		//COMPLEX OSCILLATOR
		case paramCOscKey:
		if(value<0.5)
				(cOsc->keyIn)->patch(bri->keyOut);
			else
				(cOsc->keyIn)->unpatch();
			break;

		case paramCOscPitch:
            cOsc->setPitch((double)5.0*value);
            break;

		case paramCOscPitchFine:
            cOsc->setPitchFine((double)value);
            break;

        case paramCOscPitchProc:
			cOsc->setPitchProc(3.0*(double)value);
            break;

        case paramCOscPitchProcSign:
			if(value < 0.5)
				cOsc->setPitchProcSign(-1);
			else
				cOsc->setPitchProcSign(1);
			break;


        case paramCOscTimb:
            cOsc->setTimbre((double)(0.05+0.95*value));
            break;

        case paramCOscTimbProc:
            cOsc->setTimbreProc((double)value);
            break;

        case paramCOscShape:
            cOsc->setShape((double)value);
            break;

        case paramCOscWave:
            if(value<0.33)
                cOsc->setWave(waveSpike);
            else if(value>0.66)
				cOsc->setWave(waveTriangle);
            else
                cOsc->setWave(waveSquare);
            break;

		//LOW PASS GATE 1
        case paramLpg1Offset:
            lpg1->setOffset((double)value);
            break;

        case paramLpg1Proc:
            lpg1->setProcessing((double)value);
            break;

        case paramLpg1Mode:
            if(value<0.33)
                lpg1->setMode(lpgModeFilter);
            else if(value>0.66)
                lpg1->setMode(lpgModeAmp);
            else
                lpg1->setMode(lpgModeCombo);
            break;

		//LOW PASS GATE 2
		case paramLpg2AudioSrc:
            if(value<0.33)
				(lpg2->audioIn)->patch(pre->preampOut);		//EXT
            else if(value>0.66)
				(lpg2->audioIn)->patch(lpg1->audioOut);	//LPG1
            else
				(lpg2->audioIn)->patch(mOsc->cvOut);	//Mod OSC
            break;	

        case paramLpg2Offset:
            lpg2->setOffset((double)value);
            break;

        case paramLpg2Proc:
            lpg2->setProcessing((double)value);
            break;

        case paramLpg2Mode:
            if(value<0.33)
                lpg2->setMode(lpgModeFilter);
            else if(value>0.66)
                lpg2->setMode(lpgModeAmp);
            else
                lpg2->setMode(lpgModeCombo);
            break;

		case paramPortTime:
			port->setTime((long)(sampleRate*exp(2.3-8.52*value)));
			break;

		case paramKeyShift:
		    if(value<0.33)
				key->setShift(shiftOctave);
            else if(value>0.66)
                key->setShift(shiftPreset);
            else
                key->setShift(shiftNone);
            break;

		case paramKeyPreset1:
		case paramKeyPreset2:
		case paramKeyPreset3:
			key->setPresetLevel((double)value, index-paramKeyPreset1);
			break;

		case paramPreampGain:
		    if(value<0.33)
				pre->setGain(preampGainLow);
            else if(value>0.66)
                pre->setGain(preampGainHigh);
            else
                pre->setGain(preampGainMid);
            break;

		case paramOutGainA:
			out->setGainA((double)value);
			break;

		case paramOutGainB:
			out->setGainB((double)value);
			break;

		case paramOutReverb:
	//		rev->setWet((double)value);
	//		rev->setDry((double)1.0-value);
			break;

		case paramOutMaster:
			out->setGainMaster((double)value);
			break;

		case inBPulse:			//patch routings
		case inBPress:
		case inBKey:
		case inPulPeriod:
		case inMOscIndex:
		case inMOscFreq:
		case inCOscPitch:
		case inCOscTimb:
		case inLpg1Mod:
		case inLpg2Mod:
		case inInv:
		case inKPorta:
			int i = (int)floorf(value*(totalOuts+1))-1;

			if(i == -1)
				getInput(index)->unpatch();
			else
				getInput(index)->patch(getOutput(i));

   }
}

float Easel::getParameter (VstInt32 index)
{
    return param[index];
}

void Easel::getParameterLabel (VstInt32 index, char* label)
{
    vst_strncpy (label, "", kVstMaxParamStrLen);
}

void Easel::getParameterDisplay (VstInt32 index, char* text)
{
	float value = param[index];

    switch(index)
    {
        case paramPulPeriod:    //timescales
        case paramEnvAttack:
        case paramEnvDuration:
        case paramEnvDecay:
		case paramPortTime:
            float2string ((float)exp(2.3-8.52*value), text, kVstMaxParamStrLen);
            break;

		case paramPulMode:	
            if(value<0.33)
				vst_strncpy (text, "Trig", kVstMaxParamStrLen);
            else if(value>0.66)
				vst_strncpy (text, "One", kVstMaxParamStrLen);
            else
				vst_strncpy (text, "Off", kVstMaxParamStrLen);
			break;

		case paramEnvMode:			
            if(value<0.33)
				vst_strncpy (text, "Sustained", kVstMaxParamStrLen);
            else if(value>0.66)
				vst_strncpy (text, "Off", kVstMaxParamStrLen);
            else
				vst_strncpy (text, "Transient", kVstMaxParamStrLen);
            break;		

		case paramRanTrigSrc:	//trig sources
		case paramSeqTrigSrc:
            if(value<0.33)
                vst_strncpy (text, "Keyboard", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Off", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Pulser", kVstMaxParamStrLen);
            break;

		case paramPulTrigSrc:	//more trig sources
		case paramEnvTrigSrc:
            if(value<0.33)
                vst_strncpy (text, "Keyboard", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Sequencer", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Pulser", kVstMaxParamStrLen);
            break;

		case paramSeqPul1:			//on or off
		case paramSeqPul2:
		case paramSeqPul3:
		case paramSeqPul4:
		case paramSeqPul5:
		case paramMOscKey:
		case paramCOscKey:
			if(value<0.5)
                vst_strncpy (text, "On", kVstMaxParamStrLen);
            else
				vst_strncpy (text, "Off", kVstMaxParamStrLen);
			break;

		case paramCOscPitchProcSign:		//+ or -
			if(value<0.5)
                vst_strncpy (text, "-", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "+", kVstMaxParamStrLen);
			break;

		case paramLpg1Offset:       //0-10
        case paramLpg2Offset:      
        case paramCOscTimb:
        case paramCOscShape:
		case paramSeqLev1:
		case paramSeqLev2:
		case paramSeqLev3:
		case paramSeqLev4:
		case paramSeqLev5:
		case paramOutGainA:
		case paramOutGainB:
		case paramOutReverb:
		case paramOutMonitor:
		case paramOutMaster:
		case paramKeyPreset1:
		case paramKeyPreset2:
		case paramKeyPreset3:
			float2string ((float)(10.0*value), text, kVstMaxParamStrLen);
            break;

        case paramLpg1Mode:
        case paramLpg2Mode:
            if(value<0.33)
                vst_strncpy (text, "Filter", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Amp", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Both", kVstMaxParamStrLen);
            break;

        case paramMOscWave:
            if(value<0.33)
                vst_strncpy (text, "/|/|", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "/\\/\\", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "-_-_", kVstMaxParamStrLen);
            break;

        case paramCOscWave:
            if(value<0.33)
				vst_strncpy (text, "|_|_", kVstMaxParamStrLen);
			else if(value>0.66)
                vst_strncpy (text, "/\\/\\", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "-_-_", kVstMaxParamStrLen);
            break;

        case paramSeqStages:
            if(value<0.33)
                vst_strncpy (text, "3", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "5", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "4", kVstMaxParamStrLen);
            break;

        case paramMOscDest:
            if(value<0.33)
                vst_strncpy (text, "Am Ext", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Fm Osc", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Am Osc", kVstMaxParamStrLen);
            break;

		case paramLpg2AudioSrc:
            if(value<0.33)
                vst_strncpy (text, "Ext", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Lpg 1", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Mod Osc", kVstMaxParamStrLen);
            break;

		case paramKeyShift:
            if(value<0.33)
                vst_strncpy (text, "Octave", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "Preset", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "None", kVstMaxParamStrLen);
            break;

		case paramPreampGain:
            if(value<0.33)
                vst_strncpy (text, "Low", kVstMaxParamStrLen);
            else if(value>0.66)
                vst_strncpy (text, "High", kVstMaxParamStrLen);
            else
                vst_strncpy (text, "Mid", kVstMaxParamStrLen);
            break;

        case paramCOscPitch:	//frequency - hi range
            float2string (55*pow(2, 5*value), text, kVstMaxParamStrLen);
            break;

        case paramMOscFreq:		//frequency - lo range
            float2string ((float)(0.16*pow(2, 8*value)), text, kVstMaxParamStrLen);
            break;

		case paramMOscIndex:	//0.0 - 1.0
			float2string (value, text, kVstMaxParamStrLen);
			break;

		case paramCOscPitchFine:	//what do?
		case paramMOscFreqFine:	

		case paramPulPeriodProc:	//processing; unscaled
		case paramMOscFreqProc:
		case paramMOscIndexProc:
		case paramCOscPitchProc:
		case paramCOscTimbProc:
		case paramLpg1Proc:
		case paramLpg2Proc:
            vst_strncpy (text, "", kVstMaxParamStrLen);
			break;

		case inBPulse:			//patch routings
		case inBPress:
		case inBKey:
		case inPulPeriod:
		case inMOscIndex:
		case inMOscFreq:
		case inCOscPitch:
		case inCOscTimb:
		case inLpg1Mod:
		case inLpg2Mod:
		case inInv:
		case inKPorta:
			int i = (int)floorf(value*(totalOuts+1))-1;
			
			switch(i)
			{
				case -1:
					vst_strncpy (text, "Unpatched", kVstMaxParamStrLen);
					break;
				case outRan1:			
				case outRan2:
				case outRan3:
				case outRan4:
	                vst_strncpy (text, "Rand", kVstMaxParamStrLen);
					break;
				case outSeq1:
				case outSeq2:
	                vst_strncpy (text, "Seq", kVstMaxParamStrLen);
					break;
				case outEnv1:
				case outEnv2:
				case outEnv3:
	                vst_strncpy (text, "EnvGen", kVstMaxParamStrLen);
					break;
				case outPul1:
				case outPul2:
				case outPul3:
	                vst_strncpy (text, "Pulser", kVstMaxParamStrLen);
					break;
				case outPress1:
				case outPress2:
				case outPress3:
				case outPress4:
	                vst_strncpy (text, "Pressure", kVstMaxParamStrLen);
					break;
				case outInv:
	                vst_strncpy (text, "Inverter", kVstMaxParamStrLen);
					break;
				case outMOsc:
	                vst_strncpy (text, "ModOsc", kVstMaxParamStrLen);
					break;
				case outEnvDet:
	                vst_strncpy (text, "EnvDet", kVstMaxParamStrLen);
					break;
				case outKPulse1:
				case outKPulse2:
	                vst_strncpy (text, "KeyPulse", kVstMaxParamStrLen);
					break;
				case outKPress1:
				case outKPress2:
	                vst_strncpy (text, "KeyPressure", kVstMaxParamStrLen);
					break;
				case outKPorta1:
				case outKPorta2:
	                vst_strncpy (text, "Porta", kVstMaxParamStrLen);
					break;
				case outKKey1:
				case outKKey2:
	                vst_strncpy (text, "Key", kVstMaxParamStrLen);
					break;
				case outKPreset1:
				case outKPreset2:
	                vst_strncpy (text, "KeyPreset", kVstMaxParamStrLen);	
			}	
    }
}

void Easel::getParameterName (VstInt32 index, char* text)
{
   switch(index)
    {
		case paramRanTrigSrc:	vst_strncpy (text, "RanTrigSrc", kVstMaxParamStrLen);	break;

		case paramSeqTrigSrc:	vst_strncpy (text, "SeqTrigSrc", kVstMaxParamStrLen);	break;
		case paramSeqStages:	vst_strncpy (text, "SeqStages", kVstMaxParamStrLen);	break;
		case paramSeqPul1:	vst_strncpy (text, "SeqPul1", kVstMaxParamStrLen);	break;
		case paramSeqPul2:	vst_strncpy (text, "SeqPul2", kVstMaxParamStrLen);	break;
		case paramSeqPul3:	vst_strncpy (text, "SeqPul3", kVstMaxParamStrLen);	break;
		case paramSeqPul4:	vst_strncpy (text, "SeqPul4", kVstMaxParamStrLen);	break;
		case paramSeqPul5:	vst_strncpy (text, "SeqPul5", kVstMaxParamStrLen);	break;
		case paramSeqLev1:	vst_strncpy (text, "SeqLev1", kVstMaxParamStrLen);	break;
		case paramSeqLev2:	vst_strncpy (text, "SeqLev2", kVstMaxParamStrLen);	break;
		case paramSeqLev3:	vst_strncpy (text, "SeqLev3", kVstMaxParamStrLen);	break;
		case paramSeqLev4:	vst_strncpy (text, "SeqLev4", kVstMaxParamStrLen);	break;
		case paramSeqLev5:	vst_strncpy (text, "SeqLev5", kVstMaxParamStrLen);	break;

		case paramEnvTrigSrc:	vst_strncpy (text, "EnvTrigSrc", kVstMaxParamStrLen);	break;
		case paramEnvMode:	vst_strncpy (text, "EnvMode", kVstMaxParamStrLen);	break;
    	case paramEnvAttack:	vst_strncpy (text, "EnvAttack", kVstMaxParamStrLen);	break;
    	case paramEnvDuration:	vst_strncpy (text, "EnvDuration", kVstMaxParamStrLen);	break;
    	case paramEnvDecay:	vst_strncpy (text, "EnvDecay", kVstMaxParamStrLen);	break;

		case paramPulTrigSrc:	vst_strncpy (text, "PulTrigSrc", kVstMaxParamStrLen);	break;
		case paramPulMode:	vst_strncpy (text, "PulMode", kVstMaxParamStrLen);	break;
    	case paramPulPeriod:	vst_strncpy (text, "PulPeriod", kVstMaxParamStrLen);	break;
    	case paramPulPeriodProc:	vst_strncpy (text, "PulPeriodProc", kVstMaxParamStrLen);	break;

		case paramMOscDest:	vst_strncpy (text, "MOscDest", kVstMaxParamStrLen);	break;
		case paramMOscKey:	vst_strncpy (text, "MOscKey", kVstMaxParamStrLen);	break;
		case paramMOscFreq:	vst_strncpy (text, "MOscFreq", kVstMaxParamStrLen);	break;
		case paramMOscFreqFine:	vst_strncpy (text, "MOscFreqFine", kVstMaxParamStrLen);	break;
		case paramMOscFreqProc:	vst_strncpy (text, "MOscFreqProc", kVstMaxParamStrLen);	break;
		case paramMOscIndex:	vst_strncpy (text, "MOscIndex", kVstMaxParamStrLen);	break;
		case paramMOscIndexProc:	vst_strncpy (text, "MOscIndexProc", kVstMaxParamStrLen);	break;
		case paramMOscWave:	vst_strncpy (text, "MOscWave", kVstMaxParamStrLen);	break;

		case paramCOscKey:	vst_strncpy (text, "COscKey", kVstMaxParamStrLen);	break;
    	case paramCOscPitch:	vst_strncpy (text, "COscPitch", kVstMaxParamStrLen);	break;
    	case paramCOscPitchFine:	vst_strncpy (text, "COscPitchFine", kVstMaxParamStrLen);	break;
    	case paramCOscPitchProc:	vst_strncpy (text, "COscPitchProc", kVstMaxParamStrLen);	break;
    	case paramCOscPitchProcSign:	vst_strncpy (text, "COscPitchProcSign", kVstMaxParamStrLen);	break;
    	case paramCOscTimb:	vst_strncpy (text, "COscTimb", kVstMaxParamStrLen);	break;
    	case paramCOscTimbProc:	vst_strncpy (text, "COscTimbProc", kVstMaxParamStrLen);	break;
    	case paramCOscShape:	vst_strncpy (text, "COscShape", kVstMaxParamStrLen);	break;
    	case paramCOscWave:	vst_strncpy (text, "COscWave", kVstMaxParamStrLen);	break;

    	case paramLpg1Offset:	vst_strncpy (text, "Lpg1Offset", kVstMaxParamStrLen);	break;
    	case paramLpg1Proc:	vst_strncpy (text, "Lpg1Proc", kVstMaxParamStrLen);	break;
    	case paramLpg1Mode:	vst_strncpy (text, "Lpg1Mode", kVstMaxParamStrLen);	break;

		case paramLpg2AudioSrc:	vst_strncpy (text, "Lpg2AudioSrc", kVstMaxParamStrLen);	break;
    	case paramLpg2Offset:	vst_strncpy (text, "Lpg2Offset", kVstMaxParamStrLen);	break;
    	case paramLpg2Proc:	vst_strncpy (text, "Lpg2Proc", kVstMaxParamStrLen);	break;
    	case paramLpg2Mode:	vst_strncpy (text, "Lpg2Mode", kVstMaxParamStrLen);	break;

		case paramPortTime: vst_strncpy (text, "PortTime", kVstMaxParamStrLen);	break; 
		case paramKeyShift: vst_strncpy (text, "KeyShift", kVstMaxParamStrLen);	break;
		case paramKeyPreset1: vst_strncpy (text, "KeyPreset1", kVstMaxParamStrLen);	break;
		case paramKeyPreset2: vst_strncpy (text, "KeyPreset2", kVstMaxParamStrLen);	break;
		case paramKeyPreset3: vst_strncpy (text, "KeyPreset3", kVstMaxParamStrLen);	break;
		case paramPreampGain: vst_strncpy (text, "PreampGain", kVstMaxParamStrLen);	break;
		case paramOutGainA: vst_strncpy (text, "OutGainA", kVstMaxParamStrLen);	break;
		case paramOutGainB: vst_strncpy (text, "OutGainB", kVstMaxParamStrLen);	break;
		case paramOutReverb: vst_strncpy (text, "Reverb", kVstMaxParamStrLen);	break;
		case paramOutMonitor: vst_strncpy (text, "Monitor", kVstMaxParamStrLen);	break;
		case paramOutMaster: vst_strncpy (text, "Master", kVstMaxParamStrLen);	break;

		case inBPulse: vst_strncpy (text, "inBPulse", kVstMaxParamStrLen);	break; 
		case inBPress: vst_strncpy (text, "inBPress", kVstMaxParamStrLen);	break; 
		case inBKey: vst_strncpy (text, "inBKey", kVstMaxParamStrLen);	break; 
		case inPulPeriod: vst_strncpy (text, "inPulPeriod", kVstMaxParamStrLen);	break; 
		case inMOscIndex: vst_strncpy (text, "inMOscIndex", kVstMaxParamStrLen);	break; 
		case inMOscFreq: vst_strncpy (text, "inMOscFreq", kVstMaxParamStrLen);	break; 
		case inCOscPitch: vst_strncpy (text, "inCOscPitch", kVstMaxParamStrLen);	break; 
		case inCOscTimb: vst_strncpy (text, "inCOscTimb", kVstMaxParamStrLen);	break; 
		case inLpg1Mod: vst_strncpy (text, "inLpg1Mod", kVstMaxParamStrLen);	break; 
		case inLpg2Mod: vst_strncpy (text, "inLpg2Mod", kVstMaxParamStrLen);	break; 
		case inInv: vst_strncpy (text, "inInv", kVstMaxParamStrLen);	break; 
		case inKPorta: vst_strncpy (text, "inKPorta", kVstMaxParamStrLen);

    }
}


void Easel::setProgram (VstInt32 index)
{
	if((index >= 0) && (index < NUMPROGRAMS))
	{
		currentProg = index;
		for(int i=0; i<totalParams; i++)
		{
			setParameter(i, program[currentProg]->param[i]);
		}	

		((EaselEditor*)editor)->updateAllControls();
	}
}

VstInt32 Easel::getProgram()
{
	return currentProg;
}

void Easel::setProgramName (char* name)
{
	vst_strncpy (program[currentProg]->name, name, kVstMaxProgNameLen);
}

void Easel::getProgramName (char* name)
{
	vst_strncpy (name, program[currentProg]->name, kVstMaxProgNameLen);
}

bool Easel::getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text)
{
	if((index >= 0) && (index < NUMPROGRAMS))
	{
		vst_strncpy (text, program[index]->name, kVstMaxProgNameLen);
		return true;
	}
	return false;
}

VstInt32 Easel::canDo(char* text)
{
 	if (!strcmp (text, "receiveVstEvents"))
		return 1;
	if (!strcmp (text, "receiveVstMidiEvent"))
		return 1;
	return -1;
}

VstInt32 Easel::getNumMidiInputChannels()
{
	return 1;
}


VstInt32 Easel::getNumMidiOutputChannels()
{
	return 0;
}


VstInt32 Easel::processEvents (VstEvents* ev)
{
    for(VstInt32 i = 0; i < ev->numEvents; i++)
    {
        if(ev->events[i]->type == kVstMidiType)
        {

            VstMidiEvent* event = (VstMidiEvent*)ev->events[i];
            VstInt32 status = event->midiData[0] & 0xf0;	// ignoring channel
            VstInt32 note = event->midiData[1];
			VstInt32 data = event->midiData[2];

            if(status == 0x90)		//note on
            {
				if((note >= 60) && (note < 60+Keyboard::numKeys))	//60 = middle c
					key->setPressure(note - 60, (double)data/127);
				else if((note < 63+Keyboard::numKeys) && (data > 0))
					key->setPreset(note - (60+Keyboard::numKeys));

            }
            else if(status == 0x80)  //note off
            {
				if((note >= 60) && (note < 60+Keyboard::numKeys))
					key->setPressure(note - 60, 0.0);
            }
			else if(status == 0xa0)	//aftertouch
			{
				if((note >= 60) && (note < 60+Keyboard::numKeys))
					key->setPressure(note - 60, (double)data/127);		
			
			}

        }
    }

    return 1;
}

bool Easel::getEffectName (char* name)
{
    vst_strncpy (name, "Music Easel", kVstMaxEffectNameLen);
    return true;
}

bool Easel::getVendorString (char* text)
{
    vst_strncpy (text, "Thomas Ash", kVstMaxVendorStrLen);
    return true;
}

bool Easel::getProductString (char* text)
{
    vst_strncpy (text, "Music Easel", kVstMaxProductStrLen);
    return true;
}

VstInt32 Easel::getVendorVersion()
{
    return 1;
}

Input* Easel::getInput(int index)
{
	switch(index)
	{
		case inBPulse: return bri->pulseIn; 
		case inBPress: return bri->pressureIn;
		case inBKey: return bri->keyIn;
		case inPulPeriod: return pul->periodModIn;
		case inMOscIndex: return mOsc->indexModIn;
		case inMOscFreq: return mOsc->fModIn;
		case inCOscPitch: return cOsc->expFModIn;
		case inCOscTimb: return cOsc->timbreModIn;
		case inLpg1Mod: return lpg1->controlIn;
		case inLpg2Mod: return lpg2->controlIn;
		case inInv: return inv->levelIn;
		case inKPorta: return port->levelIn;
	}
	
	return NULL;
}

Output* Easel::getOutput(int index)
{
	switch(index)
	{
		case outRan1:
		case outRan2:
		case outRan3:
		case outRan4:
			return ran->levelOut;

		case outSeq1:
		case outSeq2:
			return seq->levelOut;

		case outEnv1:
		case outEnv2:
		case outEnv3:
			return env->envOut;

		case outPul1:
		case outPul2:
		case outPul3:
			return pul->rampOut;

		case outPress1:
		case outPress2:
		case outPress3:
		case outPress4:
			return bri->pressureOut;

		case outInv:
			return inv->levelOut;

		case outMOsc:
			return mOsc->cvOut;

		case outEnvDet:
			return pre->envDetOut;

		case outKPulse1:
		case outKPulse2:
			return key->pulseOut;

		case outKPress1:
		case outKPress2:
			return key->pressureOut;

		case outKPorta1:
		case outKPorta2:
			return port->levelOut;

		case outKKey1:
		case outKKey2:
			return key->keyOut;

		case outKPreset1:
		case outKPreset2:
			return key->presetOut;
	}

	return NULL;
}

Output* Easel::getLedOutput(int index)
{
	switch(index)
	{
		case ledEnv:
			return env->ledOut;
		case ledPul:
			return pul->ledOut;
		case ledLpg1:
			return lpg1->ledOut;
		case ledLpg2:
			return lpg2->ledOut;
		case ledSeq1:
			return seq->ledOut[0];
		case ledSeq2:
			return seq->ledOut[1];
		case ledSeq3:
			return seq->ledOut[2];
		case ledSeq4:
			return seq->ledOut[3];
		case ledSeq5:
			return seq->ledOut[4];
	}

	return NULL;
}

void Easel::clickInput(int index)
{
	if(holdingOutput != -1)
	{
		setParameter(index, (float)(holdingOutput+1)/(totalOuts+1));
		holdingInput = holdingOutput = -1;	
	}
	else
	{
		holdingInput = index;
		holdingOutput = -1;	
	}
}

void Easel::doubleClickInput(int index)
{
	setParameter(index, 0.0);
	holdingInput = holdingOutput = -1;	
}

void Easel::clickOutput(int index)
{
	if(holdingInput != -1)
	{
		setParameter(holdingInput, (float)(index+1)/(totalOuts+1));
		holdingInput = holdingOutput = -1;	
	}
	else
	{
		holdingInput = -1;
		holdingOutput = index;	
	}
}