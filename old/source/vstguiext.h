//	vstguiext.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Non-standard GUI widgets:
// - a more authentically styled knob
// - LEDs
// - patch points and patch cords

#ifndef __vstguiext__
#define __vstguiext__

#include <vstgui.h>
//#include <vstcontrols.h>

#include "module.h"

enum
{
	vectorHandleTri = 0,
	vectorHandleQuad
};

class CVectorKnob : public CKnob
{
	public: 
		CVectorKnob(const CRect& size, CControlListener* listener, int32_t tag, CBitmap* background,
			float inInnerRadius, float inOuterRadius, float inHandleWidth, 
			CColor inColorHandle, int inShapeHandle);
		~CVectorKnob();
		
	protected:
		virtual void drawHandle (CDrawContext* pContext);	//override this to draw polygon instead of sprite
															// = whole point of this class
		float innerRadius;
		float outerRadius;
		float handleWidth;

		int shapeHandle;

		CPoint poly[4];
};

class CLed : public CView
{
	public:
		CLed(const CRect& size, Output* inSource);	//size is the size of the bloom effect around the LED
		~CLed();
		virtual void draw(CDrawContext *pContext);	//gets a sample from the Output* and draws an LED of
													//proportional brightness w/ pseudo-bloom effect
	protected:
		Output* source;
		CRect actualSize;	//size of the LED itself; 10x10 in the centre of the view
};

class CPatchCords : public CView
{
	public:
		CPatchCords(const CRect& size);
		~CPatchCords();

		void addInput(const CRect& area, int tag);	//new patchpoints must make themselves known to an instance of
		void addOutput(const CRect& area, int tag);	//this class, so it knows where to draw the patchcords

		virtual void draw(CDrawContext *pContext);	//current patch status is derived from Easel::getParameter
													//tags are the common reference for both operations

	protected:
		bool getInPoint(CPoint& point, int tag);	//calculates centre point 
		bool getOutPoint(CPoint& point, int tag);

		int numIns;
		int numOuts;
		int* inTag;
		int* outTag;
		CRect* inArea;
		CRect* outArea;
};

class CPatchPointIn : public CView
{
	public:
		CPatchPointIn(const CRect& size, int inTag, CPatchCords* inCords, CBitmap* background);
		~CPatchPointIn();

		CMouseEventResult onMouseDown(CPoint& where, const long& buttons);	//Informs Easel so it can update the
																			//parameters; also tells CPatchCords
																			//to refresh

	protected:
		int tag;
		CPatchCords* cords;
};

class CPatchPointOut : public CView
{
	public:
		CPatchPointOut(const CRect& size, int inTag, CPatchCords* cords, CBitmap* background);
		~CPatchPointOut();

		CMouseEventResult onMouseDown(CPoint& where, const long& buttons);	//Informs Easel so it can update the
																			//parameters; also tells CPatchCords
																			//to refresh

	protected:
		int tag;
		CPatchCords* cords;
};

#endif