//	io.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Input & Output modules
// - preamp & envelope detector
// - keyboard (MIDI is processed by Easel class)
// - output mixer
// - bridge: dummy module connecting the 3 patchpoints in 
//		the bottom left of the 208 to the internal buss

#include <cstdlib>
#include <cmath>

#include "module.h"

#include "lowpassgate.h"	//for Vactrol

#include "io.h"

//PreampDet
//=========

PreampDet::PreampDet(int inGain) : Module ()
{
	setGain(inGain);

	buffer = NULL;
	bufSize = 0;

	opto = new Vactrol();

	preampSig = 0.0;
	envDetSig = 0.0;

	ampModIn = new Input();

	preampOut = new Output(this, &preampSig);
	envDetOut = new Output(this, &envDetSig);
}

PreampDet::~PreampDet()
{
	delete opto;

	delete ampModIn;
	delete preampOut;
	delete envDetOut;
}

void PreampDet::setGain(int inGain)
{
	if(inGain == preampGainHigh)
		gain = preampGainHigh;
	else if(inGain == preampGainMid)
		gain = preampGainMid;
	else
		gain = preampGainLow;
}

void PreampDet::setBuffer(double* inBuffer, long inBufSize)
{
	buffer = inBuffer;
	bufSize = inBufSize;
}

void PreampDet::process()
{
	double in = 0.0;

	if(bufSize > 0)
	{
		in = *buffer++;
		bufSize--;
	}

	if(gain==preampGainMid)
		in*=2.0;
	else if (gain==preampGainHigh)
		in*=3.0;

	envDetSig = opto->getSample(abs(in));
	preampSig = in*(1.0-ampModIn->getSample());	
}

//Keyboard
//========

Keyboard::Keyboard(int inShift)
{
	setShift(inShift);

	for(int i=0; i<3; i++)
		presetLevel[i] = 0.0;

	key = -1;

	for(int i=0; i<numKeys; i++)
		pressure[i] = 0.0;

	preset = 0;

	opto = new Vactrol();

	pulseSig = pressureSig = keySig = presetSig = 0.0;

	pulseOut = new Output(this, &pulseSig);
	pressureOut = new Output(this, &pressureSig);
	keyOut = new Output(this, &keySig);
	presetOut = new Output(this, &presetSig);
}

Keyboard::~Keyboard()
{
	delete opto;

	delete pulseOut;
	delete pressureOut;
	delete keyOut;
	delete presetOut;
}

void Keyboard::setShift(int inShift)
{
	if(inShift == shiftPreset)
		shift = shiftPreset;
	else if(inShift == shiftOctave)
		shift = shiftOctave;
	else
		shift = shiftNone;
}

void Keyboard::setPresetLevel(double inPresetLevel, int index)
{
	if((index >= 0) && (index < 3))
		presetLevel[index] = inPresetLevel;
}

void Keyboard::setPressure(int inKey, double inPressure)
{
	if((inKey >= 0) && (inKey < numKeys))
		pressure[inKey] = inPressure;
}

void Keyboard::setPreset(int inPreset)
{
	if((inPreset >= 0) && (inPreset < 3))
		preset = inPreset;
}

void Keyboard::process()
{
	presetSig = presetLevel[preset];

	double maxPressure = 0.0;

	for(int i=0; i<numKeys; i++)
	{
		if(pressure[i] > maxPressure)
		{
			maxPressure = pressure[i];
			key = i;
		}
	}

	if(maxPressure == 0.0)
	{
		pulseSig = 0.0;
		pressureSig = opto->getSample(0.0);		//...and keySig stays the same
	}
	else
	{
		pulseSig = 1.0;
		pressureSig = opto->getSample(pressure[key]);
		keySig = (double)key / 12.0;

		if(shift==shiftOctave)
			keySig+=1.0;
		else if(shift==shiftPreset)
			keySig+=presetSig;
	}
}

//OutputMix
//=========

OutputMix::OutputMix(double inGainA, double inGainB, double inGainMaster)
{
	setGainA(inGainA);
	setGainB(inGainB);
	setGainMaster(inGainMaster);

	audioSig = 0.0;

	audioAIn = new Input();
	audioBIn = new Input();

	audioOut = new Output(this, &audioSig);
}

OutputMix::~OutputMix()
{
	delete audioAIn;
	delete audioBIn;
	delete audioOut;
}

void OutputMix::setGainA(double inGainA)
{
	if(inGainA < 0.0)
		gainA = 0.0;
	else if(inGainA > 1.0)
		gainA = 1.0;
	else
		gainA = inGainA;
}

void OutputMix::setGainB(double inGainB)
{
	if(inGainB < 0.0)
		gainB = 0.0;
	else if(inGainB > 1.0)
		gainB = 1.0;
	else
		gainB = inGainB;
}

void OutputMix::setGainMaster(double inGainMaster)
{
	if(inGainMaster < 0.0)
		gainMaster = 0.0;
	else if(inGainMaster > 1.0)
		gainMaster = 1.0;
	else
		gainMaster = inGainMaster;
}

void OutputMix::process()
{
	audioSig = ((gainA * audioAIn->getSample()) + (gainB * audioBIn->getSample())) * gainMaster;
}

//Bridge
//======

Bridge::Bridge()
{
	keySig = pulseSig = pressureSig = 0.0;

	keyIn = new Input();
	pulseIn = new Input();
	pressureIn = new Input();

	keyOut = new Output(this, &keySig);
	pulseOut = new Output(this, &pulseSig);
	pressureOut = new Output(this, &pressureSig);
}

Bridge::~Bridge()
{
	delete keyIn;
	delete pulseIn;
	delete pressureIn;

	delete keyOut;
	delete pulseOut;
	delete pressureOut;
}

void Bridge::process()
{
	keySig = keyIn->getSample();
	pulseSig = pulseIn->getSample();
	pressureSig = pressureIn->getSample();
}