//	cv.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Control Voltage modules
// - Pulser
// - Random Voltage Generator
// - Envelope Generator
// - Sequencer
// - Inverter
// - Portamento

#ifndef __cv__
#define __cv__

#include "lowpassgate.h" //for Vactrol

#include "module.h"

enum
{
    envModeTransient = 0,
    envModeSustained,
	envModeOff
};

enum
{
    envStageAttack = 0,
    envStageSustain,
    envStageDecay,
    envStageOff
};

enum 
{
	pulModeTrig = 0,
	pulModeOff,
	pulModeOne
};

class PulseGen		//NOT the 208 pulser; this is a utility class that generates short pulses
{
	public:
		PulseGen();
		~PulseGen();

		void fire();		//fires off a pulse if there isn't one already in progress

		double getSample();		//return sample & tick

	protected:
		static const int length = 100;		//or whatever
		int counter;

};

class OnePole		//basic 1-pole IIR filter
{
	public:
		OnePole(long inTime = 65536);		
		~OnePole();

		void setTime(long inTime);	//time constant instead of cutoff freq; more useful for control voltage

		double getSample(double x_0);	//return sample & tick

	protected:
		double a_0;
		double b_1;

		double y_1;
};

class Random : public Module
{
    public:
        Random();
        ~Random();

        Input* trigIn;      //input

        Output* levelOut;    //output

        void process();

    protected:
        double trig_1;  //internal state

        double levelSig;    //output
};

class Sequencer : public Module
{
    public:
        Sequencer(int inStages = 5);
        ~Sequencer();

        void setStages(int inStages);			  //VST params
        void setPulse(bool inPulse, int index);
        void setLevel(double inLevel, int index);

        Input* trigIn;      //inputs

        Output* pulseOut;    //outputs
        Output* levelOut;
		Output* ledOut[5];

        void process();



    protected:
        int stages;     //VST params
        bool pulse[5];
        double level[5];

        int current;       //internal state
        double trig_1;
		PulseGen* pulseGen;
		Vactrol* opto[5];

        double pulseSig;    //outputs
        double levelSig;
		double ledSig[5];

};

class Pulser : public Module
{
    public:
        Pulser(long inPeriod = 65536, double inPeriodProc = 0.0, int inMode = pulModeTrig);
        ~Pulser();

        void setPeriod(long inPeriod);				  //vst params
        void setPeriodProc(double inPeriodProc);
		void setMode(int inMode);

        Input* trigIn;			//inputs
        Input* periodModIn;

        Output* pulseOut;		//outputs
        Output* rampOut;
        Output* ledOut;

        void process();

    protected:
        long period;        //vst params
        double periodProc;
		int mode;

		PulseGen* pulseGen;		//internal state
		Vactrol* opto;
        double trig_1;
        double inc;

        double pulseSig;	//output signals
        double rampSig;
		double ledSig;
};

class EnvGen : public Module
{
    public:
        EnvGen(long inAttack = 65536, long inDuration = 65536, long inDecay = 65536, int inMode = envModeOff);
        ~EnvGen();

        void setAttack(long inAttack);
        void setDuration(long inDuration);
        void setDecay(long inDecay);
        void setMode(int inMode);

        Input* trigIn;      //inputs

        Output* envOut;      //outputs
        Output* ledOut;

        void process();

    protected:
        long attack;        //VST params
        long duration;
        long decay;
        int mode;

        int stage;          //internal state
        double level;
        double inc;
        double trig_1;
		Vactrol* opto;

        double envSig;      //output signals
		double ledSig;
};

class Inverter : public Module
{
	public:
		Inverter();
		~Inverter();

		Input* levelIn;

		Output* levelOut;

		void process();

	protected:
		double levelSig;
};

class Portamento : public Module
{
	public:
		Portamento(long inTime = 65536);
		~Portamento();

		void setTime(long inTime);

		Input* levelIn;

		Output* levelOut;

		void process();

	protected:
		long time;

		OnePole* filter;

		double levelSig;
};

#endif // __cv__
