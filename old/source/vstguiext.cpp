//	vstguiext.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Non-standard GUI widgets:
// - a more authentically styled knob
// - LEDs
// - patch points and patch cords

#include <cmath>
#include <cstdlib>

#include <plugin-bindings/aeffguieditor.h>
#include <vstgui.h>
//#include <vstcontrols.h>

#include "easel.h"

#include "module.h"

#include "vstguiext.h"

#define EASEL ((Easel*)(((AEffGUIEditor*)getEditor())->getEffect()))


//CVectorKnob
//===========

CVectorKnob::CVectorKnob(const CRect &size, CControlListener *listener, int32_t tag, CBitmap *background,
						 float inInnerRadius, float inOuterRadius, float inHandleWidth, 
						 CColor inColorHandle, int inShapeHandle)
: CKnob (size, listener, tag, background, NULL)
{
	innerRadius = inInnerRadius;
	outerRadius = inOuterRadius;
	handleWidth = inHandleWidth / 2;
	setColorHandle(inColorHandle);
	if(inShapeHandle == vectorHandleTri)
		shapeHandle = vectorHandleTri;
	else
		shapeHandle = vectorHandleQuad;

	setStartAngle((float)(4.0*M_PI/3.0));
	setRangeAngle((float)(-5.0*M_PI/3.0));
}

CVectorKnob::~CVectorKnob()
{

}

void CVectorKnob::drawHandle(VSTGUI::CDrawContext *pContext)		
{
	float alpha = (value - bCoef) / aCoef;
	float sinAlpha = sinf(alpha);
	float cosAlpha = cosf(alpha);

	CPoint origin ((size.left + size.right)/2, (size.top + size.bottom)/2);

	pContext->setFillColor(colorHandle);

	if(shapeHandle == vectorHandleTri)
	{
		poly[0].x = (CCoord)(origin.x + (cosAlpha*outerRadius));
		poly[0].y = (CCoord)(origin.y - (sinAlpha*outerRadius));

		poly[1].x = (CCoord)(origin.x + (cosAlpha*innerRadius) - (sinAlpha*handleWidth));
		poly[1].y = (CCoord)(origin.y - (sinAlpha*innerRadius) - (cosAlpha*handleWidth));

		poly[2].x = (CCoord)(origin.x + (cosAlpha*innerRadius) + (sinAlpha*handleWidth));
		poly[2].y = (CCoord)(origin.y - (sinAlpha*innerRadius) + (cosAlpha*handleWidth));
		
		pContext->drawPolygon(poly, 3, kDrawFilled);//, true);	
	}
	else
	{
		poly[0].x = (CCoord)(origin.x + (cosAlpha*outerRadius) - (sinAlpha*handleWidth));
		poly[0].y = (CCoord)(origin.y - (sinAlpha*outerRadius) - (cosAlpha*handleWidth));

		poly[1].x = (CCoord)(origin.x + (cosAlpha*innerRadius) - (sinAlpha*handleWidth));
		poly[1].y = (CCoord)(origin.y - (sinAlpha*innerRadius) - (cosAlpha*handleWidth));

		poly[2].x = (CCoord)(origin.x + (cosAlpha*innerRadius) + (sinAlpha*handleWidth));
		poly[2].y = (CCoord)(origin.y - (sinAlpha*innerRadius) + (cosAlpha*handleWidth));

		poly[3].x = (CCoord)(origin.x + (cosAlpha*outerRadius) + (sinAlpha*handleWidth));
		poly[3].y = (CCoord)(origin.y - (sinAlpha*outerRadius) + (cosAlpha*handleWidth));
		
		pContext->drawPolygon(poly, 4, kDrawFilled);//, true);		
	}
}

//CLed
//====

CLed::CLed(const CRect& size, Output* inSource) : CView(size)
{
	actualSize.left = (size.left + size.right)/2 - 5;
	actualSize.top = (size.top + size.bottom)/2 - 5;
	actualSize.right = (size.left + size.right)/2 + 5;
	actualSize.bottom = (size.top + size.bottom)/2 + 5;

	source = inSource;
	this->setMouseEnabled(false);
}

CLed::~CLed()
{

}

void CLed::draw(CDrawContext *pContext)
{
	CColor color = kBlackCColor;
	double i = 0.0;

	if(source)
		i = source->getSampleNoUpdate();

 	if(i<0.75)
		color.red = (unsigned char)(i*340);
	else
		color.red = 255;


	pContext->setFillColor(color);
	pContext->drawEllipse(actualSize, kDrawFilled);

	if(i>0.75)
	{	
		color = kRedCColor;
		color.alpha = (unsigned char)((i-0.75)*340);

		pContext->setFillColor(color);

		CRect bloom;

		bloom.left = actualSize.left;
		bloom.top = size.top;
		bloom.right = actualSize.right;
		bloom.bottom = size.bottom;

		pContext->drawEllipse(bloom, kDrawFilled);

		bloom.left = size.left;
		bloom.top = actualSize.top;
		bloom.right = size.right;
		bloom.bottom = actualSize.bottom;

		pContext->drawEllipse(bloom, kDrawFilled);

	}

	this->setDirty();
}

//CPatchCords 
//===========

CPatchCords::CPatchCords(const CRect& size) : CView(size)
{
	numIns = numOuts = 0;
	inTag = outTag = NULL;
	inArea = outArea = NULL;

	setMouseEnabled(false);
}
CPatchCords::~CPatchCords()
{
	if(inTag)
		delete[] inTag;
	if(outTag)
		delete[] outTag;
	if(inArea)
		delete[] inArea;
	if(outArea)
		delete[] outArea;
}

void CPatchCords::addInput(const CRect& area, int tag)
{
    int* tempTag = new int[numIns+1];
    CRect* tempArea = new CRect[numIns+1];

    for(int i=0; i<numIns; i++)
	{
        tempTag[i] = inTag[i];
		tempArea[i] = inArea[i];
	}

    tempTag[numIns] = tag;
	tempArea[numIns] = area;

	if(inTag)
		delete[] inTag;
	if(inArea)
		delete[] inArea;

	inTag = tempTag;
	inArea = tempArea;

	numIns++;
}

void CPatchCords::addOutput(const CRect& area, int tag)
{
    int* tempTag = new int[numOuts+1];
    CRect* tempArea = new CRect[numOuts+1];

    for(int i=0; i<numOuts; i++)
	{
        tempTag[i] = outTag[i];
		tempArea[i] = outArea[i];
	}

    tempTag[numOuts] = tag;
	tempArea[numOuts] = area;

	if(outTag)
		delete[] outTag;
	if(outArea)
		delete[] outArea;

	outTag = tempTag;
	outArea = tempArea;

	numOuts++;
}

void CPatchCords::draw(CDrawContext *pContext)
{
	CPoint from;
	CPoint to;

	pContext->setLineWidth(5);
	pContext->setFrameColor(kBlackCColor);
	
	for(int i=0; i<numIns; i++)
	{
		int source = (int)floorf(EASEL->getParameter(inTag[i])*(totalOuts+1))-1;

		if(getInPoint(from, inTag[i]) && getOutPoint(to, source))
		{
			pContext->moveTo(from);
			pContext->lineTo(to);
		}
	}
}

bool CPatchCords::getInPoint(CPoint& point, int tag)
{
	for(int i=0; i<numIns; i++)
	{
		if(inTag[i] == tag)
		{
			point.x = (inArea[i].left + inArea[i].right)/2;
			point.y = (inArea[i].top + inArea[i].bottom)/2;
			return true;
		}
	}

	return false;
}

bool CPatchCords::getOutPoint(CPoint& point, int tag)
{
	for(int i=0; i<numOuts; i++)
	{
		if(outTag[i] == tag)
		{
			point.x = (outArea[i].left + outArea[i].right)/2;
			point.y = (outArea[i].top + outArea[i].bottom)/2;
			return true;
		}
	}

	return false;
}


//CPatchPointIn 
//=============
CPatchPointIn::CPatchPointIn(const CRect& size, int inTag, CPatchCords* inCords, CBitmap* background) : CView(size)
{
	tag = inTag;
	cords = inCords;
	cords->addInput(size, tag);
	setBackground(background);
}

CPatchPointIn::~CPatchPointIn()
{

}

CMouseEventResult CPatchPointIn::onMouseDown(VSTGUI::CPoint &where, const long &buttons)
{
	if(buttons & kDoubleClick)
	{
		EASEL->doubleClickInput(tag);
		
	}
	else
	{
		EASEL->clickInput(tag);	
	}

	cords->setDirty();
	return kMouseEventHandled;
}

//CPatchPointOut
//==============

CPatchPointOut::CPatchPointOut(const CRect& size, int inTag, CPatchCords* inCords, CBitmap* background) : CView(size)
{
	tag = inTag;
	cords = inCords;
	cords->addOutput(size, tag);
	setBackground(background);
}

CPatchPointOut::~CPatchPointOut()
{

}

CMouseEventResult CPatchPointOut::onMouseDown(VSTGUI::CPoint &where, const long &buttons)
{

	EASEL->clickOutput(tag);	
	cords->setDirty();

	return kMouseEventHandled;
}