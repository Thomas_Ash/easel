//	reverbmodel.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//	FFT convolution reverb using embedded impulse

#ifndef __reverbmodel__
#define __reverbmodel__

#include <fftw3.h>

class ReverbModel
{
	public:
		ReverbModel(double inWet = 0.0, double inDry = 0.0);
		~ReverbModel();

		void setWet(double inWet);
		void setDry(double inDry);

		void process(double* in, double* out, long size);

	protected:
		void loadImpulse();
		void setIoSize(long inIoSize);
		void deallocate();
	
		double wet;
		double dry;

		long impSize;		//when impulse is loaded, we don't know the host buffer size  
		double* impTime;	//and therefore neither the fft size. so use real numbers

		long ioSize;		//host tells us this. keep track in case it changes...
		long fftSize;		//...because if it does, so does this...

		//...and all these must be re-allocated

		double* fftTime;		//used to zero pad impulse and also audio

		fftw_complex* impFreq;		//fft of the zero padded impulse 
		fftw_complex* audioFreq;		//fft of the audio being reverberated

		double* overlap;		//add to next set of outputs (have to lose imaginary part sometime so do it asap)

		fftw_plan forwardPlan;
		fftw_plan inversePlan;

};

#endif