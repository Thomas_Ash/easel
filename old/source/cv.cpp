//	cv.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Control Voltage modules
// - Pulser
// - Random Voltage Generator
// - Envelope Generator
// - Sequencer
// - Inverter
// - Portamento

#include "module.h"
#include "cv.h"
#include <cstdlib>
#include <ctime>
#include <cmath>

//PulseGen
//========

PulseGen::PulseGen()
{
	counter = 0;
}

PulseGen::~PulseGen()
{

}

void PulseGen::fire()
{
	if(counter == 0)
		counter = length;
}

double PulseGen::getSample()
{
	if(counter > 0)
	{
		counter--;
		return 1.0;
	}

	return 0.0;
}

//OnePole
//=======

OnePole::OnePole(long inTime)
{
	setTime(inTime);
	a_0 = b_1 = y_1 = 0.0;
}

OnePole::~OnePole()
{
	
}

void OnePole::setTime(long inTime)
{
	if(inTime<1)
		inTime=1;

	b_1 = exp(-1.0/inTime);
	a_0 = 1.0-b_1;
}

double OnePole::getSample(double x_0)
{
	double y_0 = a_0*x_0 + b_1*y_1;

	y_1 = y_0;

	return y_0;

}

//Random
//======

Random::Random() : Module ()
{
    srand((int)time(NULL));

    trig_1 = 0.0;

	levelSig = 0.0;

    trigIn = new Input();

    levelOut = new Output(this, &levelSig);


}

Random::~Random()
{
	delete trigIn;
	delete levelOut;
}

void Random::process()
{
    double trig = trigIn->getSample();
    if((trig > 0.1) && (trig_1 <= 0.1))    //rising edge
    {

        levelSig = (double)(rand())/RAND_MAX;
    }

    trig_1 = trig;
}

//Sequencer
//=========

Sequencer::Sequencer(int inStages) : Module ()
{
    setStages(inStages);

    for(int i=0; i<5; i++)
    {
        pulse[i] = false;
        level[i] = 0.0;

		ledSig[i] = 0.0;
		opto[i] = new Vactrol();
		ledOut[i] = new Output(this, &(ledSig[i]));
    }

    current = 0;
    trig_1 = 0.0;
	pulseGen = new PulseGen();

    pulseSig = 0.0;
    levelSig = 0.0;

    trigIn = new Input();

	pulseOut = new Output(this, &pulseSig);
	levelOut = new Output(this, &levelSig);

}

Sequencer::~Sequencer()
{
	delete pulseGen;
	delete trigIn;
	delete pulseOut;
	delete levelOut;
}

void Sequencer::setStages(int inStages)
{
    if(inStages <= 3)
        stages = 3;
    else if(inStages >= 5)
        stages = 5;
    else
        stages = 4;
}

void Sequencer::setPulse(bool inPulse, int index)
{
    if((index >= 0)&&(index < 5))
        pulse[index] = inPulse;
}

void Sequencer::setLevel(double inLevel, int index)
{
    if((index >= 0)&&(index < 5))
        level[index] = inLevel;
}

void Sequencer::process()
{
    double trig = trigIn->getSample();

    if((trig > 0.1) && (trig_1 <= 0.1))    //rising edge
    {
        current = (current+1)%stages;

        if(pulse[current])
        {
            pulseGen->fire();
        }
    }

    trig_1 = trig;

	pulseSig = pulseGen->getSample();

    levelSig = level[current];

	for(int i=0; i<5; i++)
    {
		ledSig[i] = opto[i]->getSample((current==i) ? 1.0 : 0.0);
	}
}


//Pulser
//======

Pulser::Pulser(long inPeriod, double inPeriodProc, int inMode) : Module()
{
    setPeriod(inPeriod);
    setPeriodProc(inPeriodProc);
	setMode(inMode);

	pulseGen = new PulseGen();
    inc = -1.0/period;;

    pulseSig = 0.0;
    rampSig = 1.0;
	ledSig = 0.0;

    trigIn = new Input();
    periodModIn = new Input();

    pulseOut = new Output(this, &pulseSig);
    rampOut = new Output(this, &rampSig);
	ledOut = new Output(this, &ledSig);

	opto = new Vactrol();
}

Pulser::~Pulser()
{
	delete pulseGen;
	delete trigIn;
	delete periodModIn;
	delete pulseOut;
	delete rampOut;
	delete ledOut;
	delete opto;
}

void Pulser::setPeriod(long inPeriod)
{
    if(inPeriod<1)
        period = 1;
    else
        period = inPeriod;

    if(rampSig > 0.0)
        inc = -1.0/period;
}

void Pulser::setPeriodProc(double inPeriodProc)
{
    if(inPeriodProc < 0.0)
        periodProc = 0.0;
    else if(inPeriodProc > 3.0)
        periodProc = 3.0;
    else
        periodProc = inPeriodProc;
}

void Pulser::setMode(int inMode)
{
	if(inMode == pulModeTrig)
		mode = pulModeTrig;
	else if(inMode == pulModeOff)
		mode = pulModeOff;
	else
		mode = pulModeOne;
}

void Pulser::process()
{
    double trig = 0.0;

	if(mode == pulModeTrig)
		trig = trigIn->getSample();
	else if(mode == pulModeOne)
		trig = 1.0;

    if((trig > 0.1) && (trig_1 <= 0.1))    //rising edge
    {
        rampSig = 1.0;
        inc = -1.0/period;
    }

    trig_1 = trig;

    rampSig+=(inc*exp(periodModIn->getSample()*periodProc));

    if(rampSig < 0.0)
    {
        rampSig = 0.0;
		pulseGen->fire();
    }

	pulseSig = pulseGen->getSample();
	ledSig = opto->getSample(rampSig);
}

//EnvGen
//======

EnvGen::EnvGen(long inAttack, long inDuration, long inDecay, int inMode) : Module()
{
    stage = envStageOff;
    level = 0.0;
    inc = 0.0;
	trig_1 = 0.0;

    setAttack(inAttack);
    setDuration(inDuration);
    setDecay(inDecay);
    setMode(inMode);

    envSig = 0.0;
	ledSig = 0.0;

    trigIn = new Input();

    envOut = new Output(this, &envSig);
	ledOut = new Output(this, &ledSig);

	opto = new Vactrol();
}

EnvGen::~EnvGen()
{
	delete trigIn;
	delete envOut;
	delete ledOut;
	delete opto;
}

void EnvGen::setAttack(long inAttack)
{
    if(inAttack < 1)
        attack = 1;
    else
        attack = inAttack;

    if(stage==envStageAttack)
        inc = 1.0/attack;
}

void EnvGen::setDuration(long inDuration)
{
    if(inDuration < 1)
        duration = 1;
    else
        duration = inDuration;

    if((stage==envStageSustain)&&(mode==envModeTransient))
        inc = -1.0/duration;
}

void EnvGen::setDecay(long inDecay)
{
    if(inDecay < 1)
        decay = 1;
    else
        decay = inDecay;

    if(stage==envStageDecay)
        inc = -1.0/decay;
}

void EnvGen::setMode(int inMode)
{
    if(inMode==envModeSustained)
        mode = envModeSustained;
    else if(inMode==envModeTransient)
        mode = envModeTransient;
    else
        mode = envModeOff;
}


void EnvGen::process()
{
	double trig = trigIn->getSample();

	if(mode==envModeSustained)
	{
		if(trig > 0.1)            //gate on
		{
			if((stage==envStageOff)||(stage==envStageDecay))
			{
				stage = envStageAttack;
				inc = 1.0/attack;
			}
		}
		else            //gate off
		{
			if((stage==envStageSustain)||(stage==envStageAttack))
			{
				stage = envStageDecay;
				inc = -1.0/decay;
			}
		}
	}
	else if (mode==envModeTransient)
	{
		if((trig > 0.1) && (trig_1 <= 0.1))    //rising edge
		{
			if((stage==envStageOff)||(stage==envStageDecay))
			{
				stage = envStageAttack;
				inc = 1.0/attack;
			}		
		}
	}



    level+=inc;
	trig_1 = trig;

    if(level > 1.0)   //attack finished
    {
        level = 1.0;
        stage = envStageSustain;

        if(mode==envModeTransient)
            inc = -1.0/duration;
        else
            inc = 0.0;
    }

    if(level < 0.0)     //decay finished OR transient mode sustain finished
    {
        if(stage==envStageSustain)
        {
            level = 1.0;
            stage = envStageDecay;
            inc = -1.0/decay;
        }
        else
        {
            level = 0.0;
            stage = envStageOff;
            inc = 0.0;
        }

    }

    if(stage == envStageSustain)
        envSig = 1.0;
    else
        envSig = level;

	ledSig = opto->getSample(envSig);
}

//Inverter
//========

Inverter::Inverter() : Module()
{
	levelSig = 0.0;
	levelIn = new Input();
	levelOut = new Output(this, &levelSig);
}

Inverter::~Inverter()
{
	delete levelIn;
	delete levelOut;
}

void Inverter::process()
{
	levelSig = 1.0 - levelIn->getSample();
}

//Portamento
//==========

Portamento::Portamento(long inTime) : Module()
{
	filter = new OnePole();

	setTime(inTime);

	levelSig = 0.0;
	levelIn = new Input();
	levelOut = new Output(this, &levelSig);
}

Portamento::~Portamento()
{
	delete filter;
	delete levelIn;
	delete levelOut;
}

void Portamento::setTime(long inTime)
{
	time = (inTime < 1) ? 1 : inTime;
	filter->setTime(time);
}

void Portamento::process()
{
	levelSig = filter->getSample(levelIn->getSample());
}