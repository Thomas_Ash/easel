//	oscillators.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Complex and modulation oscillators
// - bandlimited impulse train oscillator 
// - simple phase accumulator & sine oscillators
// - harmonic generation & integrating waveshapers
// - FM and AM

#ifndef _USE_MATH_DEFINES
#define _USE_MATH_DEFINES
#endif


#include <cmath>
#include <cstdlib>

#include "module.h"

#include "oscillators.h"

//LeakyIntegrator
//===============

LeakyIntegrator::LeakyIntegrator(double in_b_1)
{
    b_1 = in_b_1;
    y_1 = 0.0;
}

LeakyIntegrator::~LeakyIntegrator()
{

}

double LeakyIntegrator::getSample(double x_0)
{
    double y_0 = x_0 + b_1*y_1;
    y_1 = y_0;
    return y_0;
}

//Lookup
//======

Lookup::Lookup(int inSize)
{
    size = inSize;
    k_0 = new double[size];
    k_1 = new double[size];
}

Lookup::~Lookup()
{
    delete[] k_0;
    delete[] k_1;
}

double Lookup::getValue(double x)
{
    if((x<0.0)||(x>=1.0))
        x -= floor(x);

    double h = x*size;
    int i = (int)floor(h);
    h = h-i;

    return k_0[i] + h*k_1[i];
}

void Lookup::interpolate()
{
    int i, i_1;

    for(i=0; i<size; i++)
    {
        i_1 = (i+1)%size;
        k_1[i] = (k_0[i_1] - k_0[i])/size;
    }
}

//BlackmanWindow
//==============

BlackmanWindow::BlackmanWindow(int inSize)
: Lookup(inSize)
{
    int i;

    for(i=0; i<size; i++)
    {
        k_0[i] = 0.42 - 0.5*cos((2*M_PI*i)/size) + 0.08*cos((4*M_PI*i)/size);
    }

    interpolate();
}

BlackmanWindow::~BlackmanWindow()
{

}

//SineLookup
//==========

SineLookup::SineLookup(int inSize)
: Lookup(inSize)
{
    int i;

    for(i=0; i<size; i++)
    {
        k_0[i] = sin((2*M_PI*i)/size);
    }

    interpolate();

}

SineLookup::~SineLookup()
{

}

//TimbreLookup
//============

TimbreLookup::TimbreLookup(int inSize)
: Lookup(inSize)
{
    int i;
    double x;

    for(i=0; i<size; i++)
    {
        x = (2.0*i)/size - 1.0;

        k_0[i] = sin(M_PI*x*8)/(1.2*(1+exp(fabs(18*x)-6))) + 1/(1+exp(6-12*x)) - 1/(1+exp(6+12*x));
    }

    interpolate();

}

TimbreLookup::~TimbreLookup()
{

}

//SincKernel
//==========

BlackmanWindow SincKernel::window(LOOKUPSIZE);

SincKernel::SincKernel(double offset)
{
    x = offset-(KERNELSIZE/2);
    sinx = sin(M_PI*offset);
}

SincKernel::~SincKernel()
{

}

bool SincKernel::accSample(double* sample)  //returns true when it is played out
{
    (*sample) += (window.getValue(x/KERNELSIZE+0.5) * ((x == 0.0) ? 1.0 : sinx / (M_PI*x)));


    x+=1.0;
    sinx = 0.0-sinx;

    return(x >= KERNELSIZE);
}

//Oscillator
//==========

Oscillator::Oscillator(double inFreq, double inPhase)
{
    setFreq(inFreq);

    if((inPhase<0.0)||(inPhase>=1.0))
        inPhase -= floor(inPhase);

    phase = inPhase;
}

Oscillator::~Oscillator()
{

}

void Oscillator::setFreq(double inFreq)
{
    if((inFreq>0.0)&&(inFreq<=0.5))
        freq = inFreq;
    else
        freq = 0.000000001;     
}

double Oscillator::getSample()
{
    phase+=freq;
    if(phase>=1.0)
        phase -= 1.0;

    return phase;
}

//BlitOsc
//=======

BlitOsc::BlitOsc(double inFreq, double inPhase)
: Oscillator (inFreq, inPhase)
{
    for(int i=0; i<KERNELSIZE; i++)
        kernels[i] = NULL;
}

BlitOsc::~BlitOsc()
{
    for(int i=0; i<KERNELSIZE; i++)
    {
        if(kernels[i])
            delete kernels[i];
    }
}

double BlitOsc::getSample()
{
    int j;
    double sample = 0.0;

    for(j=0; j<KERNELSIZE; j++)
    {
        if(kernels[j])
        {
            if((kernels[j])->accSample(&sample))
            {
                delete kernels[j];
                kernels[j] = NULL;
            }
        }
    }

    phase+=freq;
    if(phase>=1.0)
    {
        phase -= 1.0;

        j=0;
        while(kernels[j])
            j++;

        kernels[j] = new SincKernel(phase/freq);

    }

    return sample;
}


//SineOsc
//=======

SineOsc::SineOsc(double inFreq, double inPhase)
: Oscillator (inFreq, inPhase)
{

}

SineOsc::~SineOsc()
{

}

double SineOsc::getSample()
{
    double sample = sin(2*M_PI*phase);

    phase+=freq;
    if(phase>=1.0)
        phase -= 1.0;

    return sample;
}

//ComplexOsc
//==========

ComplexOsc::ComplexOsc(double inPitch, double inPitchFine, double inPitchProc, int inPitchProcSign, 
					   double inTimbre, double inTimbreProc, double inShape, int inWave) : Module ()
{
    setPitch(inPitch);
	setPitchFine(inPitchFine);
    setPitchProc(inPitchProc);
	setPitchProcSign(inPitchProcSign);
    setTimbre(inTimbre);
    setTimbreProc(inTimbreProc);
    setShape(inShape);
    setWave(inWave);

    blit1 = new BlitOsc(0.01, 0.0);
    blit2 = new BlitOsc(0.01, 0.5);
    sine = new SineOsc(0.01, 0.0);

    blit2square = new LeakyIntegrator(0.99);
    square2tri = new LeakyIntegrator(0.99);

    timbrefunc = new TimbreLookup(LOOKUPSIZE);

    spike = square = tri = 0.0;
	
	audioSig = 0.0;

    keyIn = new Input();
	expFModIn = new Input();
	linFModIn = new Input();
	timbreModIn = new Input();
	ampModIn = new Input();
	
    audioOut = new Output(this, &audioSig);
}

ComplexOsc::~ComplexOsc()
{
    delete blit1;
    delete blit2;
    delete sine;

    delete blit2square;
    delete square2tri;

    delete timbrefunc;

	delete keyIn;
	delete expFModIn;
	delete linFModIn;
	delete timbreModIn;
	delete ampModIn;

	delete audioOut;
}

void ComplexOsc::setPitch(double inPitch)
{
    if(inPitch < 0.0)
        pitch = 0.0;
    else if(inPitch > 5.0)
        pitch = 5.0;
    else
        pitch = inPitch;
}

void ComplexOsc::setPitchFine(double inPitchFine)
{
    if(inPitchFine < 0.0)
        pitchFine = 0.0;
    else if(inPitchFine > 1.0)
        pitchFine = 1.0;
    else
        pitchFine = inPitchFine;
}

void ComplexOsc::setPitchProc(double inPitchProc)
{
    if(inPitchProc < 0.0)
        pitchProc = 0.0;
    else if(inPitchProc > 3.0)
        pitchProc = 3.0;
    else
        pitchProc = inPitchProc;
}

void ComplexOsc::setPitchProcSign(int inPitchProcSign)
{
    if(inPitchProcSign == -1)
        pitchProcSign = -1;
    else
        pitchProcSign = 1;
}


void ComplexOsc::setTimbre(double inTimbre)
{
    if(inTimbre < 0.05)
        timbre = 0.05;
    else if(inTimbre > 1.0)
        timbre = 1.0;
    else
        timbre = inTimbre;
}

void ComplexOsc::setTimbreProc(double inTimbreProc)
{
    if(inTimbreProc < 0.00)
        timbreProc = 0.00;
    else if(inTimbreProc > 1.0)
        timbreProc = 1.0;
    else
        timbreProc = inTimbreProc;
}

void ComplexOsc::setShape(double inShape)
{
    if(inShape < 0.0)
        shape = 0.0;
    else if(inShape > 1.0)
        shape = 1.0;
    else
        shape = inShape;
}

void ComplexOsc::setWave(int inWave)
{
    if((inWave>=0)&&(inWave<3))
        wave = inWave;
    else
        wave = 0;
}

void ComplexOsc::process()
{
    double f = 0.00124716553 * pow(2,
        (pitch + pitchFine/12 + pitchProc*pitchProcSign*expFModIn->getSample() + keyIn->getSample()))
        + linFModIn->getSample()*0.03;

    blit1->setFreq(f);
    blit2->setFreq(f);
    sine->setFreq(f);

    spike = blit1->getSample();
    square = blit2square->getSample(spike - blit2->getSample());
    tri = square2tri->getSample(square*f*8.48);

    double timbreDrive = timbre + timbreProc*(timbreModIn->getSample());

    if(timbreDrive > 1.0)
        timbreDrive = 1.0;

    audioSig = timbrefunc->getValue(0.5 + (sine->getSample()*timbreDrive/2)) * (1.0-shape);

    switch (wave)
    {
        case waveSpike:
            audioSig += spike*shape*2;
            break;

        case waveSquare:
            audioSig += square*shape;
            break;

        case waveTriangle:
            audioSig += timbrefunc->getValue(0.5 + (tri*timbreDrive/2))*shape;
            break;
    }

    audioSig *= (1.0-ampModIn->getSample());
}

//ModulationOsc
//=============

ModulationOsc::ModulationOsc(double inFreq, double inFreqFine, double inFreqProc, 
							 double inIndex, double inIndexProc, int inWave) : Module ()
{
	setFreq(inFreq);
	setFreqFine(inFreqFine);
	setFreqProc(inFreqProc);
	setIndex(inIndex);
	setIndexProc(inIndexProc);
	setWave(inWave);

    blit1 = new BlitOsc(0.01, 0.0);
    blit2 = new BlitOsc(0.01, 0.5);
	simple = new Oscillator(0.01, 0.0);

    blit2saw = new LeakyIntegrator(0.99);
    blit2square = new LeakyIntegrator(0.99);
    square2tri = new LeakyIntegrator(0.99);

	phase = spike = saw = square = tri = 0.0;

    cvSig = fmSig = amSig = 0.0;

    keyIn = new Input();
	fModIn = new Input();
	indexModIn = new Input();
	
	cvOut = new Output(this, &cvSig);
	fmOut = new Output(this, &fmSig);
	amOut = new Output(this, &amSig);
}

ModulationOsc::~ModulationOsc()
{
	delete blit1;
	delete blit2;
	delete simple;

	delete blit2saw;
	delete blit2square;
	delete square2tri;

	delete keyIn;
	delete fModIn;
	delete indexModIn;

	delete cvOut;
	delete fmOut;
	delete amOut;
}

void ModulationOsc::setFreq(double inFreq)
{
    if(inFreq < 0.0)
        freq = 0.0;
    else if(inFreq > 8.0)		
        freq = 8.0;
    else
        freq = inFreq;
}

void ModulationOsc::setFreqFine(double inFreqFine)
{
    if(inFreqFine < 0.0)
        freqFine = 0.0;
    else if(inFreqFine > 1.0)		
        freqFine = 1.0;
    else
        freqFine = inFreqFine;
}

void ModulationOsc::setFreqProc(double inFreqProc)
{
    if(inFreqProc < 0.00)
        freqProc = 0.00;
    else if(inFreqProc > 3.0)
        freqProc = 3.0;
    else
        freqProc = inFreqProc;
}

void ModulationOsc::setIndex(double inIndex)
{
    if(inIndex < 0.0)
        index = 0.0;
    else if(inIndex > 1.0)		
        index = 1.0;
    else
        index = inIndex;
}

void ModulationOsc::setIndexProc(double inIndexProc)
{
    if(inIndexProc < 0.0)
        indexProc = 0.0;
    else if(inIndexProc > 1.0)		
        indexProc = 1.0;
    else
        indexProc = inIndexProc;
}

void ModulationOsc::setWave(int inWave)
{
    if((inWave>0)&&(inWave<=3))
        wave = inWave;
    else
        wave = 3;
}

void ModulationOsc::process()
{
	double f = freq + freqFine/3 + freqProc*fModIn->getSample() + keyIn->getSample();	//f first used for log of frequency...

	double mix = f/8;					//mix = gain of bandlimited osc = 1-gain of simple osc
	mix = (mix < 0.0) ? 0.0 : mix;
	mix = (mix > 1.0) ? 1.0 : mix;

	f = (3.62811791 * pow(2, f)) / 1000000;	//...then actual frequency

    blit1->setFreq(f);
    blit2->setFreq(f);
	simple->setFreq(f);

	phase = simple->getSample();
	spike = blit1->getSample();
    saw = blit2saw->getSample(spike-f);
    square = blit2square->getSample(spike - blit2->getSample());
    tri = square2tri->getSample(square*f*8.48);

    switch (wave)
    {
        case waveSawtooth:
            cvSig = 2*mix*saw + (1-mix)*(phase * 2 - 1);
            break;

        case waveSquare:
			cvSig = 2*mix*square + (1-mix)*((phase > 0.5) ? -1 : 1);
            break;

        case waveTriangle:
            cvSig = 2*mix*tri + (1-mix)*(abs(phase-0.5) * 4 - 1);
            break;
    }

    double i = index + indexProc*indexModIn->getSample();

	i = (i>1.0) ? 1.0 : i;

	fmSig = cvSig*i;
	amSig = fmSig + i;
}
