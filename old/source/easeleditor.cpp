//	easeleditor.cpp
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Graphical User Interface

#include <audioeffectx.h>
#include <vstgui.h>
#include <plugin-bindings/aeffguieditor.h>

#include "vstguiext.h"

#include "easeleditor.h"
#include "easel.h"	//for VST parameters enum to use as control tags


//EaselEditor methods

EaselEditor::EaselEditor (void* ptr)
: AEffGUIEditor (ptr)
{
	for(int i=0; i<totalParams; i++)
		allControls[i] = NULL;

    CBitmap* bg = new CBitmap ("ID_BACKGROUND");

	rect.left = 0;
	rect.top = 0;
	rect.right = (VstInt16)(bg->getWidth());
	rect.bottom = (VstInt16)(bg->getHeight());

    bg->forget();
}

EaselEditor::~EaselEditor ()
{

}

bool EaselEditor::open (void* ptr)
{
    AEffGUIEditor::open(ptr);

	//master frame w/ background texture
    CBitmap* bg = new CBitmap ("ID_BACKGROUND");
    CRect frameSize (0, 0, bg->getWidth(), bg->getHeight());
    frame = new CFrame (frameSize, ptr, this);
    frame->setBackground(bg);
    //useOffscreen(true);
    bg->forget();

	openSliders();
	openSwitches();
	openKnobs();
	openLEDs();
	openPatchPoints(frameSize);		//frame->getSize doesn't give correct rect?!? 
	
	return true;
}

void EaselEditor::openSliders()
{
    CBitmap* slider_red = new CBitmap ("ID_SLIDER_RED");
    CBitmap* slider_blue = new CBitmap ("ID_SLIDER_BLUE");
    CBitmap* slider_black = new CBitmap ("ID_SLIDER_BLACK");
    CBitmap* slider_yellow = new CBitmap ("ID_SLIDER_YELLOW");
    CBitmap* slider_orange = new CBitmap ("ID_SLIDER_ORANGE");

	CVerticalSlider* slider;
	CRect sliderSize(0, 172, 15, 248);

	//sliders for sequencer
	sliderSize.left = 26;
	sliderSize.right = 41;
	slider = new CVerticalSlider(sliderSize, this, paramSeqLev1, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramSeqLev1));
	allControls[paramSeqLev1] = slider;
    frame->addView(slider);

	sliderSize.left = 51;
	sliderSize.right = 66;
	slider = new CVerticalSlider(sliderSize, this, paramSeqLev2, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramSeqLev2));
	allControls[paramSeqLev2] = slider;
    frame->addView(slider);

	sliderSize.left = 75;
	sliderSize.right = 90;
	slider = new CVerticalSlider(sliderSize, this, paramSeqLev3, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramSeqLev3));
	allControls[paramSeqLev3] = slider;
    frame->addView(slider);

	sliderSize.left = 100;
	sliderSize.right = 115;
	slider = new CVerticalSlider(sliderSize, this, paramSeqLev4, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramSeqLev4));
	allControls[paramSeqLev4] = slider;
    frame->addView(slider);

	sliderSize.left = 125;
	sliderSize.right = 140;
	slider = new CVerticalSlider(sliderSize, this, paramSeqLev5, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramSeqLev5));
	allControls[paramSeqLev5] = slider;
    frame->addView(slider);

	//sliders for envelope generator
	sliderSize.left = 158;
	sliderSize.right = 173;
	slider = new CVerticalSlider(sliderSize, this, paramEnvAttack, 172, 240, slider_orange, NULL);
	slider->setValue(effect->getParameter(paramEnvAttack));
	allControls[paramEnvAttack] = slider;
    frame->addView(slider);

	sliderSize.left = 194;
	sliderSize.right = 209;
	slider = new CVerticalSlider(sliderSize, this, paramEnvDuration, 172, 240, slider_orange, NULL);
	slider->setValue(effect->getParameter(paramEnvDuration));
	allControls[paramEnvDuration] = slider;
    frame->addView(slider);

	sliderSize.left = 217;
	sliderSize.right = 232;
	slider = new CVerticalSlider(sliderSize, this, paramEnvDecay, 172, 240, slider_orange, NULL);
	slider->setValue(effect->getParameter(paramEnvDecay));
	allControls[paramEnvDecay] = slider;
    frame->addView(slider);

	//sliders for pulser
	sliderSize.left = 252;
	sliderSize.right = 267;
	slider = new CVerticalSlider(sliderSize, this, paramPulPeriodProc, 172, 240, slider_yellow, NULL);
	slider->setValue(effect->getParameter(paramPulPeriodProc));
	allControls[paramPulPeriodProc] = slider;
    frame->addView(slider);

	sliderSize.left = 286;
	sliderSize.right = 301;
	slider = new CVerticalSlider(sliderSize, this, paramPulPeriod, 172, 240, slider_yellow, NULL);
	slider->setValue(effect->getParameter(paramPulPeriod));
	allControls[paramPulPeriod] = slider;
    frame->addView(slider);

	//sliders for modulation oscillator
	sliderSize.left = 321;
	sliderSize.right = 336;
	slider = new CVerticalSlider(sliderSize, this, paramMOscIndexProc, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramMOscIndexProc));
	allControls[paramMOscIndexProc] = slider;
    frame->addView(slider);

	sliderSize.left = 355;
	sliderSize.right = 370;
	slider = new CVerticalSlider(sliderSize, this, paramMOscIndex, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramMOscIndex));
	allControls[paramMOscIndex] = slider;
    frame->addView(slider);

	sliderSize.left = 390;
	sliderSize.right = 405;
	slider = new CVerticalSlider(sliderSize, this, paramMOscFreqProc, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramMOscFreqProc));
	allControls[paramMOscFreqProc] = slider;
    frame->addView(slider);

	sliderSize.left = 425;
	sliderSize.right = 440;
	slider = new CVerticalSlider(sliderSize, this, paramMOscFreq, 172, 240, slider_blue, NULL);
	slider->setValue(effect->getParameter(paramMOscFreq));
	allControls[paramMOscFreq] = slider;
    frame->addView(slider);



	//sliders for complex oscillator
	sliderSize.left = 460;
	sliderSize.right = 475;
	slider = new CVerticalSlider(sliderSize, this, paramCOscPitchProc, 172, 240, slider_red, NULL);
	slider->setValue(effect->getParameter(paramCOscPitchProc));
	allControls[paramCOscPitchProc] = slider;
    frame->addView(slider);

	sliderSize.left = 493;
	sliderSize.right = 508;
	slider = new CVerticalSlider(sliderSize, this, paramCOscPitch, 172, 240, slider_red, NULL);
	slider->setValue(effect->getParameter(paramCOscPitch));
	allControls[paramCOscPitch] = slider;
    frame->addView(slider);

	sliderSize.left = 528;
	sliderSize.right = 543;
	slider = new CVerticalSlider(sliderSize, this, paramCOscTimbProc, 172, 240, slider_red, NULL);
	slider->setValue(effect->getParameter(paramCOscTimbProc));
	allControls[paramCOscTimbProc] = slider;
    frame->addView(slider);

	sliderSize.left = 563;
	sliderSize.right = 578;
	slider = new CVerticalSlider(sliderSize, this, paramCOscTimb, 172, 240, slider_red, NULL);
	slider->setValue(effect->getParameter(paramCOscTimb));
	allControls[paramCOscTimb] = slider;
    frame->addView(slider);

	//sliders for low pass gates
	sliderSize.left = 598;
	sliderSize.right = 613;
	slider = new CVerticalSlider(sliderSize, this, paramLpg1Proc, 172, 240, slider_black, NULL);
	slider->setValue(effect->getParameter(paramLpg1Proc));
 	allControls[paramLpg1Proc] = slider;
    frame->addView(slider);

	sliderSize.left = 633;
	sliderSize.right = 648;
	slider = new CVerticalSlider(sliderSize, this, paramLpg1Offset, 172, 240, slider_black, NULL);
	slider->setValue(effect->getParameter(paramLpg1Offset));
	allControls[paramLpg1Offset] = slider;
    frame->addView(slider);

	sliderSize.left = 666;
	sliderSize.right = 681;
	slider = new CVerticalSlider(sliderSize, this, paramLpg2Proc, 172, 240, slider_black, NULL);
	slider->setValue(effect->getParameter(paramLpg2Proc));
	allControls[paramLpg2Proc] = slider;
    frame->addView(slider);

	sliderSize.left = 701;
	sliderSize.right = 716;
	slider = new CVerticalSlider(sliderSize, this, paramLpg2Offset, 172, 240, slider_black, NULL);
	slider->setValue(effect->getParameter(paramLpg2Offset));
	allControls[paramLpg2Offset] = slider;
    frame->addView(slider);


    slider_red->forget();
    slider_blue->forget();
    slider_black->forget();
    slider_yellow->forget();
    slider_orange->forget();
}

void EaselEditor::openSwitches()
{
	//switches

	CBitmap* switch_v2_red = new CBitmap ("ID_SWITCH_V2_RED");
	CBitmap* switch_v3_red = new CBitmap ("ID_SWITCH_V3_RED");
	CBitmap* switch_v2_blue = new CBitmap ("ID_SWITCH_V2_BLUE");
	CBitmap* switch_v3_blue = new CBitmap ("ID_SWITCH_V3_BLUE");
	CBitmap* switch_v3_black = new CBitmap ("ID_SWITCH_V3_BLACK");
	CBitmap* switch_v3_yellow = new CBitmap ("ID_SWITCH_V3_YELLOW");
	CBitmap* switch_v3_orange = new CBitmap ("ID_SWITCH_V3_ORANGE");
	CBitmap* switch_v3_white = new CBitmap ("ID_SWITCH_V3_WHITE");
	CBitmap* switch_h3_black = new CBitmap ("ID_SWITCH_H3_BLACK");

	CVerticalSwitch* vswitch;
	CRect switchSize;

	//switch for random generator
	switchSize.left = 119;
	switchSize.top = 287;
	switchSize.right = 129;
	switchSize.bottom = 328;
	vswitch = new CVerticalSwitch(switchSize, this, paramRanTrigSrc, 3, 41, 3, switch_v3_white);
	vswitch->setValue(effect->getParameter(paramRanTrigSrc));
	allControls[paramRanTrigSrc] = vswitch;
	frame->addView(vswitch);



	//switches for sequencer
	switchSize.left = 18;
	switchSize.top = 58;
	switchSize.right = 28;
	switchSize.bottom = 99;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqTrigSrc, 3, 41, 3, switch_v3_blue);
	vswitch->setValue(effect->getParameter(paramSeqTrigSrc));
	allControls[paramSeqTrigSrc] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 99;
	switchSize.right = 109;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqStages, 3, 41, 3, switch_v3_blue);
	vswitch->setValue(effect->getParameter(paramSeqStages));
	allControls[paramSeqStages] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 9;
	switchSize.top = 100;
	switchSize.right = 19;
	switchSize.bottom = 141;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqPul1, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramSeqPul1));
	allControls[paramSeqPul1] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 34;
	switchSize.right = 44;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqPul2, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramSeqPul2));
	allControls[paramSeqPul2] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 61;
	switchSize.right = 71;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqPul3, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramSeqPul3));
	allControls[paramSeqPul3] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 89;
	switchSize.right = 99;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqPul4, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramSeqPul4));
	allControls[paramSeqPul4] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 116;
	switchSize.right = 126;
	vswitch = new CVerticalSwitch(switchSize, this, paramSeqPul5, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramSeqPul5));
	allControls[paramSeqPul5] = vswitch;
	frame->addView(vswitch);

	//switches for envelope generator
	switchSize.left = 152;
	switchSize.top = 59;
	switchSize.right = 162;
	switchSize.bottom = 100;
	vswitch = new CVerticalSwitch(switchSize, this, paramEnvTrigSrc, 3, 41, 3, switch_v3_orange);
	vswitch->setValue(effect->getParameter(paramEnvTrigSrc));
	allControls[paramEnvTrigSrc] = vswitch;
	frame->addView(vswitch);

	switchSize.top = 100;
	switchSize.bottom = 141;
	vswitch = new CVerticalSwitch(switchSize, this, paramEnvMode, 3, 41, 3, switch_v3_orange);
	vswitch->setValue(effect->getParameter(paramEnvMode));
	allControls[paramEnvMode] = vswitch;
	frame->addView(vswitch);

	//switches for pulser
	switchSize.left = 246;
	switchSize.top = 60;
	switchSize.right = 256;
	switchSize.bottom = 101;
	vswitch = new CVerticalSwitch(switchSize, this, paramPulTrigSrc, 3, 41, 3, switch_v3_yellow);
	vswitch->setValue(effect->getParameter(paramPulTrigSrc));
	allControls[paramPulTrigSrc] = vswitch;
	frame->addView(vswitch);

	switchSize.top = 101;
	switchSize.bottom = 142;
	vswitch = new CVerticalSwitch(switchSize, this, paramPulMode, 3, 41, 3, switch_v3_yellow);
	vswitch->setValue(effect->getParameter(paramPulMode));
	allControls[paramPulMode] = vswitch;
	frame->addView(vswitch);

	//switches for modulation oscillator
	switchSize.left = 315;
	switchSize.top = 60;
	switchSize.right = 325;
	switchSize.bottom = 101;
	vswitch = new CVerticalSwitch(switchSize, this, paramMOscKey, 2, 41, 2, switch_v2_blue);
	vswitch->setValue(effect->getParameter(paramMOscKey));
	allControls[paramMOscKey] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 385;
	switchSize.right = 395;
	vswitch = new CVerticalSwitch(switchSize, this, paramMOscWave, 3, 41, 3, switch_v3_blue);
	vswitch->setValue(effect->getParameter(paramMOscWave));
	allControls[paramMOscWave] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 315;
	switchSize.top = 100;
	switchSize.right = 325;
	switchSize.bottom = 141;
	vswitch = new CVerticalSwitch(switchSize, this, paramMOscDest, 3, 41, 3, switch_v3_blue);
	vswitch->setValue(effect->getParameter(paramMOscDest));
	allControls[paramMOscDest] = vswitch;
	frame->addView(vswitch);

	//switches for complex oscillator
	switchSize.left = 455;
	switchSize.top = 60;
	switchSize.right = 465;
	switchSize.bottom = 101;
	vswitch = new CVerticalSwitch(switchSize, this, paramCOscKey, 2, 41, 2, switch_v2_red);
	vswitch->setValue(effect->getParameter(paramCOscKey));
	allControls[paramCOscKey] = vswitch;
	frame->addView(vswitch);

	switchSize.top = 101;
	switchSize.bottom = 142;
	vswitch = new CVerticalSwitch(switchSize, this, paramCOscPitchProcSign, 2, 41, 2, switch_v2_red);
	vswitch->setValue(effect->getParameter(paramCOscPitchProcSign));
	allControls[paramCOscPitchProcSign] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 524;
	switchSize.right = 534;
	vswitch = new CVerticalSwitch(switchSize, this, paramCOscWave, 3, 41, 3, switch_v3_red);
	vswitch->setValue(effect->getParameter(paramCOscWave));
	allControls[paramCOscWave] = vswitch;
	frame->addView(vswitch);

	//switches for lowpass gates
	switchSize.left = 594;
	switchSize.right = 604;
	vswitch = new CVerticalSwitch(switchSize, this, paramLpg1Mode, 3, 41, 3, switch_v3_black);
	vswitch->setValue(effect->getParameter(paramLpg1Mode));
	allControls[paramLpg1Mode] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 700;
	switchSize.right = 710;
	vswitch = new CVerticalSwitch(switchSize, this, paramLpg2Mode, 3, 41, 3, switch_v3_black);
	vswitch->setValue(effect->getParameter(paramLpg2Mode));
	allControls[paramLpg2Mode] = vswitch;
	frame->addView(vswitch);

	switchSize.left = 644;
	switchSize.top = 58;
	switchSize.right = 654;
	switchSize.bottom = 99;
	vswitch = new CVerticalSwitch(switchSize, this, paramLpg2AudioSrc, 3, 41, 3, switch_v3_black);
	vswitch->setValue(effect->getParameter(paramLpg2AudioSrc));
	allControls[paramLpg2AudioSrc] = vswitch;
	frame->addView(vswitch);

	//switch for keyboard

	switchSize.left = 483;
	switchSize.top = 368;
	switchSize.right = 493;
	switchSize.bottom = 409;
	vswitch = new CVerticalSwitch(switchSize, this, paramKeyShift, 3, 41, 3, switch_v3_red);
	vswitch->setValue(effect->getParameter(paramKeyShift));
	allControls[paramKeyShift] = vswitch;
	frame->addView(vswitch);

	//switch for preamp

	switchSize.left = 617;
	switchSize.top = 33;
	switchSize.right = 659;
	switchSize.bottom = 44;
	CHorizontalSwitch* hswitch = new CHorizontalSwitch(switchSize, this, paramPreampGain, 3, 11, 3, switch_h3_black);
	hswitch->setValue(effect->getParameter(paramPreampGain));
	allControls[paramPreampGain] = hswitch;
	frame->addView(hswitch);

	switch_v2_red->forget();
	switch_v3_red->forget();
	switch_v2_blue->forget();
	switch_v3_blue->forget();
	switch_v3_black->forget();
	switch_v3_yellow->forget();
	switch_v3_orange->forget();
	switch_v3_white->forget();
	switch_h3_black->forget();
}

void EaselEditor::openKnobs()
{
	//knobs

	CBitmap* knob_large_red = new CBitmap("ID_KNOB_LARGE_RED");
	CBitmap* knob_med_red = new CBitmap("ID_KNOB_MED_RED");
	CBitmap* knob_med_blue = new CBitmap("ID_KNOB_MED_BLUE");
	CBitmap* knob_small_red = new CBitmap("ID_KNOB_SMALL_RED");
	CBitmap* knob_small_blue = new CBitmap("ID_KNOB_SMALL_BLUE");

	CVectorKnob* knob;
	CRect knobSize;

	//knob for Mod Osc

	knobSize.left = 420;
	knobSize.top = 108;
	knobSize.right = 447;
	knobSize.bottom = 135;
	knob = new CVectorKnob(knobSize, this, paramMOscFreqFine, knob_small_blue, 
		0, 10, 2, kBlackCColor, vectorHandleQuad);
	knob->setValue(effect->getParameter(paramMOscFreqFine));
	allControls[paramMOscFreqFine] = knob;
	frame->addView(knob);

	//knobs for Complex Osc
	
	knobSize.left = 489;
	knobSize.right = 516;
	knob = new CVectorKnob(knobSize, this, paramCOscPitchFine, knob_small_red, 
		0, 10, 2, kBlackCColor, vectorHandleQuad);
	knob->setValue(effect->getParameter(paramCOscPitchFine));
	allControls[paramCOscPitchFine] = knob;
	frame->addView(knob);

	knobSize.left = 539;
	knobSize.top = 72;
	knobSize.right = 566;
	knobSize.bottom = 99;
	knob = new CVectorKnob(knobSize, this, paramCOscShape, knob_small_red, 
		0, 10, 2, kBlackCColor, vectorHandleQuad);
	knob->setValue(effect->getParameter(paramCOscShape));
	allControls[paramCOscShape] = knob;
	frame->addView(knob);

	//knobs for output mixer

	knobSize.left = 735;
	knobSize.top = 65;
	knobSize.right = 788;
	knobSize.bottom = 116;
	knob = new CVectorKnob(knobSize, this, paramOutGainA, knob_med_blue, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramOutGainA));
	allControls[paramOutGainA] = knob;
	frame->addView(knob);

	knobSize.left = 779;
	knobSize.top = 23;
	knobSize.right = 832;
	knobSize.bottom = 74;
	knob = new CVectorKnob(knobSize, this, paramOutGainB, knob_med_blue, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramOutGainB));
	allControls[paramOutGainB] = knob;
	frame->addView(knob);

	knobSize.left = 780;
	knobSize.top = 118;
	knobSize.right = 833;
	knobSize.bottom = 169;
	knob = new CVectorKnob(knobSize, this, paramOutReverb, knob_med_blue, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramOutReverb));
	allControls[paramOutReverb] = knob;
	frame->addView(knob);

	knobSize.left = 735;
	knobSize.top = 174;
	knobSize.right = 788;
	knobSize.bottom = 225;
	knob = new CVectorKnob(knobSize, this, paramOutMonitor, knob_med_blue, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramOutMonitor));
	allControls[paramOutMonitor] = knob;
	frame->addView(knob);

	knobSize.left = 749;
	knobSize.top = 245;
	knobSize.right = 817;
	knobSize.bottom = 313;
	knob = new CVectorKnob(knobSize, this, paramOutMaster, knob_large_red, 
		21, 31, 6, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramOutMaster));
	allControls[paramOutMaster] = knob;
	frame->addView(knob);

	//knob for portamento

	knobSize.left = 218;
	knobSize.top = 364;
	knobSize.right = 269;
	knobSize.bottom = 412;
	knob = new CVectorKnob(knobSize, this, paramPortTime, knob_med_red, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramPortTime));
	allControls[paramPortTime] = knob;
	frame->addView(knob);

	//knobs for presets

	knobSize.left = 632;
	knobSize.top = 365;
	knobSize.right = 683;
	knobSize.bottom = 413;
	knob = new CVectorKnob(knobSize, this, paramKeyPreset1, knob_med_red, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramKeyPreset1));
	allControls[paramKeyPreset1] = knob;
	frame->addView(knob);

	knobSize.left = 695;
	knobSize.right = 746;
	knob = new CVectorKnob(knobSize, this, paramKeyPreset2, knob_med_red, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramKeyPreset2));
	allControls[paramKeyPreset2] = knob;
	frame->addView(knob);

	knobSize.left = 757;
	knobSize.right = 808;
	knob = new CVectorKnob(knobSize, this, paramKeyPreset3, knob_med_red, 
		17, 23, 4, kWhiteCColor, vectorHandleTri);
	knob->setValue(effect->getParameter(paramKeyPreset3));
	allControls[paramKeyPreset3] = knob;
	frame->addView(knob);


	knob_large_red->forget();
	knob_med_red->forget();
	knob_med_blue->forget();
	knob_small_red->forget();
	knob_small_blue->forget();
}

void EaselEditor::openLEDs()
{
	//LEDs

	CRect ledSize;
	CLed* led;

	ledSize.left = 205;
	ledSize.top = 107;
	ledSize.right = 240;
	ledSize.bottom = 142;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledEnv));
	frame->addView(led);

	ledSize.left = 275;
	ledSize.top = 107;
	ledSize.right = 308;
	ledSize.bottom = 142;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledPul));
	frame->addView(led);

	ledSize.left = 604;
	ledSize.top = 74;
	ledSize.right = 640;
	ledSize.bottom = 110;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledLpg1));
	frame->addView(led);

	ledSize.left = 674;
	ledSize.top = 58;
	ledSize.right = 711;
	ledSize.bottom = 98;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledLpg2));
	frame->addView(led);

	ledSize.left = 10;
	ledSize.top = 110;
	ledSize.right = 46;
	ledSize.bottom = 146;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledSeq1));
	frame->addView(led);

	ledSize.left = 36;
	ledSize.top = 110;
	ledSize.right = 71;
	ledSize.bottom = 146;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledSeq2));
	frame->addView(led);

	ledSize.left = 65;
	ledSize.top = 110;
	ledSize.right = 102;
	ledSize.bottom = 146;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledSeq3));
	frame->addView(led);

	ledSize.left = 91;
	ledSize.top = 110;
	ledSize.right = 127;
	ledSize.bottom = 146;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledSeq4));
	frame->addView(led);

	ledSize.left = 117;
	ledSize.top = 110;
	ledSize.right = 155;
	ledSize.bottom = 146;
	led = new CLed(ledSize, ((Easel*)effect)->getLedOutput(ledSeq5));
	frame->addView(led);
}

void EaselEditor::openPatchPoints(const CRect& frameSize)
{
	CPatchCords* cords = new CPatchCords(frameSize);

	//patchpoints

	CBitmap* jack_red = new CBitmap("ID_JACK_RED");
	CBitmap* jack_blue = new CBitmap("ID_JACK_BLUE");
	CBitmap* jack_black = new CBitmap("ID_JACK_BLACK");
	CBitmap* jack_yellow = new CBitmap("ID_JACK_YELLOW");
	CBitmap* jack_orange = new CBitmap("ID_JACK_ORANGE");
	CBitmap* jack_white = new CBitmap("ID_JACK_WHITE");
	CBitmap* jack_purple = new CBitmap("ID_JACK_PURPLE");

	CPatchPointIn* jackIn;
	CPatchPointOut* jackOut;
	CRect jackSize;

	//main patchfield: top row
	jackSize.left = 213;
	jackSize.top = 277;
	jackSize.right = 237;
	jackSize.bottom = 300;
	jackOut = new CPatchPointOut(jackSize, outSeq1, cords, jack_blue);
	frame->addView(jackOut);

	jackSize.left = 282;
	jackSize.top = 278;
	jackSize.right = 306;
	jackSize.bottom = 301;
	jackOut = new CPatchPointOut(jackSize, outEnv1, cords, jack_orange);
	frame->addView(jackOut);

	jackSize.left = 349;
	jackSize.top = 277;
	jackSize.right = 373;
	jackSize.bottom = 300;
	jackOut = new CPatchPointOut(jackSize, outPul1, cords, jack_yellow);
	frame->addView(jackOut);

	jackSize.left = 419;
	jackSize.top = 278;
	jackSize.right = 443;
	jackSize.bottom = 301;
	jackOut = new CPatchPointOut(jackSize, outSeq2, cords, jack_blue);
	frame->addView(jackOut);

	jackSize.left = 487;
	jackSize.top = 278;
	jackSize.right = 511;
	jackSize.bottom = 301;
	jackOut = new CPatchPointOut(jackSize, outEnv2, cords, jack_orange);
	frame->addView(jackOut);

	jackSize.left = 559;
	jackSize.top = 277;
	jackSize.right = 583;
	jackSize.bottom = 300;
	jackOut = new CPatchPointOut(jackSize, outPul2, cords, jack_yellow);	
	frame->addView(jackOut);

	jackSize.left = 628;
	jackSize.top = 278;
	jackSize.right = 652;
	jackSize.bottom = 301;
	jackOut = new CPatchPointOut(jackSize, outEnv3, cords, jack_orange);
	frame->addView(jackOut);

	jackSize.left = 696;
	jackSize.top = 278;
	jackSize.right = 720;
	jackSize.bottom = 301;
	jackOut = new CPatchPointOut(jackSize, outPul3, cords, jack_yellow);	
	frame->addView(jackOut);

	//main patchfield: middle row
	jackSize.left = 247;
	jackSize.top = 291;
	jackSize.right = 271;
	jackSize.bottom = 314;
	jackIn = new CPatchPointIn(jackSize, inPulPeriod, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 316;
	jackSize.top = 291;
	jackSize.right = 340;
	jackSize.bottom = 314;
	jackIn = new CPatchPointIn(jackSize, inMOscIndex, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 385;
	jackSize.top = 291;
	jackSize.right = 409;
	jackSize.bottom = 314;
	jackIn = new CPatchPointIn(jackSize, inMOscFreq, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 455;
	jackSize.top = 292;
	jackSize.right = 479;
	jackSize.bottom = 315;
	jackIn = new CPatchPointIn(jackSize, inCOscPitch, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 523;
	jackSize.top = 292;
	jackSize.right = 547;
	jackSize.bottom = 315;
	jackIn = new CPatchPointIn(jackSize, inCOscTimb, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 593;
	jackSize.top = 293;
	jackSize.right = 617;
	jackSize.bottom = 316;
	jackIn = new CPatchPointIn(jackSize, inLpg1Mod, cords, jack_black);	
	frame->addView(jackIn);

	jackSize.left = 663;
	jackSize.top = 293;
	jackSize.right = 687;
	jackSize.bottom = 316;
	jackIn = new CPatchPointIn(jackSize, inLpg2Mod, cords, jack_black);	
	frame->addView(jackIn);



	//main patchfield: bottom row
	jackSize.left = 213;
	jackSize.top = 304;
	jackSize.right = 237;
	jackSize.bottom = 327;
	jackOut = new CPatchPointOut(jackSize, outRan1, cords, jack_white);
	frame->addView(jackOut);

	jackSize.left = 281;
	jackSize.top = 304;
	jackSize.right = 305;
	jackSize.bottom = 327;
	jackOut = new CPatchPointOut(jackSize, outPress1, cords, jack_purple);
	frame->addView(jackOut);

	jackSize.left = 350;
	jackSize.top = 304;
	jackSize.right = 374;
	jackSize.bottom = 327;
	jackOut = new CPatchPointOut(jackSize, outRan2, cords, jack_white);
	frame->addView(jackOut);

	jackSize.left = 419;
	jackSize.top = 305;
	jackSize.right = 443;
	jackSize.bottom = 328;
	jackOut = new CPatchPointOut(jackSize, outPress2, cords, jack_purple);
	frame->addView(jackOut);

	jackSize.left = 488;
	jackSize.top = 305;
	jackSize.right = 512;
	jackSize.bottom = 328;
	jackOut = new CPatchPointOut(jackSize, outRan3, cords, jack_white);
	frame->addView(jackOut);

	jackSize.left = 559;
	jackSize.top = 305;
	jackSize.right = 583;
	jackSize.bottom = 328;
	jackOut = new CPatchPointOut(jackSize, outPress3, cords, jack_purple);
	frame->addView(jackOut);

	jackSize.left = 628;
	jackSize.top = 306;
	jackSize.right = 652;
	jackSize.bottom = 329;
	jackOut = new CPatchPointOut(jackSize, outRan4, cords, jack_white);
	frame->addView(jackOut);

	jackSize.left = 696;
	jackSize.top = 306;
	jackSize.right = 720;
	jackSize.bottom = 329;
	jackOut = new CPatchPointOut(jackSize, outPress4, cords, jack_purple);
	frame->addView(jackOut);

	//Inverter

	jackSize.left = 490;
	jackSize.top = 18;
	jackSize.right = 514;
	jackSize.bottom = 41;
	jackIn = new CPatchPointIn(jackSize, inInv, cords, jack_black);
	frame->addView(jackIn);

	jackSize.left = 521;
	jackSize.top = 18;
	jackSize.right = 545;
	jackSize.bottom = 41;
	jackOut = new CPatchPointOut(jackSize, outInv, cords, jack_black);
	frame->addView(jackOut);

	//Pre

	jackSize.left = 660;
	jackSize.top = 19;
	jackSize.right = 684;
	jackSize.bottom = 42;
	jackOut = new CPatchPointOut(jackSize, outEnvDet, cords, jack_black);
	frame->addView(jackOut);

	//Mod Osc

	jackSize.left = 733;
	jackSize.top = 19;
	jackSize.right = 757;
	jackSize.bottom = 42;
	jackOut = new CPatchPointOut(jackSize, outMOsc, cords, jack_black);
	frame->addView(jackOut);

	//Bridge

	jackSize.left = 8;
	jackSize.top = 296;
	jackSize.right = 32;
	jackSize.bottom = 319;
	jackIn = new CPatchPointIn(jackSize, inBPulse, cords, jack_red);
	frame->addView(jackIn);

	jackSize.left = 45;
	jackSize.top = 296;
	jackSize.right = 69;
	jackSize.bottom = 319;
	jackIn = new CPatchPointIn(jackSize, inBPress, cords, jack_purple);
	frame->addView(jackIn);

	jackSize.left = 83;
	jackSize.top = 296;
	jackSize.right = 107;
	jackSize.bottom = 319;
	jackIn = new CPatchPointIn(jackSize, inBKey, cords, jack_black);
	frame->addView(jackIn);

	//Keyboard

	jackSize.left = 14;
	jackSize.top = 365;
	jackSize.right = 38;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPulse1, cords, jack_red);
	frame->addView(jackOut);

	jackSize.left = 53;
	jackSize.top = 365;
	jackSize.right = 77;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPulse2, cords, jack_red);
	frame->addView(jackOut);

	jackSize.left = 14;
	jackSize.top = 365;
	jackSize.right = 38;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPulse1, cords, jack_red);
	frame->addView(jackOut);

	jackSize.left = 93;
	jackSize.top = 365;
	jackSize.right = 117;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPress1, cords, jack_purple);
	frame->addView(jackOut);

	jackSize.left = 130;
	jackSize.top = 365;
	jackSize.right = 154;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPress2, cords, jack_purple);
	frame->addView(jackOut);

	jackSize.left = 177;
	jackSize.top = 365;
	jackSize.right = 201;
	jackSize.bottom = 388;
	jackIn = new CPatchPointIn(jackSize, inKPorta, cords, jack_black);
	frame->addView(jackIn);

	jackSize.left = 279;
	jackSize.top = 365;
	jackSize.right = 303;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPorta1, cords, jack_black);
	frame->addView(jackOut);

	jackSize.left = 316;
	jackSize.top = 365;
	jackSize.right = 340;
	jackSize.bottom = 388;
	jackOut = new CPatchPointOut(jackSize, outKPorta2, cords, jack_black);
	frame->addView(jackOut);

	jackSize.left = 358;
	jackSize.top = 367;
	jackSize.right = 382;
	jackSize.bottom = 390;
	jackOut = new CPatchPointOut(jackSize, outKKey1, cords, jack_black);
	frame->addView(jackOut);

	jackSize.left = 396;
	jackSize.top = 367;
	jackSize.right = 420;
	jackSize.bottom = 390;
	jackOut = new CPatchPointOut(jackSize, outKKey2, cords, jack_black);
	frame->addView(jackOut);

	jackSize.left = 561;
	jackSize.top = 368;
	jackSize.right = 585;
	jackSize.bottom = 391;
	jackOut = new CPatchPointOut(jackSize, outKPreset1, cords, jack_black);
	frame->addView(jackOut);

	jackSize.left = 599;
	jackSize.top = 368;
	jackSize.right = 623;
	jackSize.bottom = 391;
	jackOut = new CPatchPointOut(jackSize, outKPreset2, cords, jack_black);
	frame->addView(jackOut);




	frame->addView(cords);

	jack_red->forget();
	jack_blue->forget();
	jack_black->forget();
	jack_yellow->forget();
	jack_orange->forget();
	jack_white->forget();
	jack_purple->forget();		
}


void EaselEditor::close ()
{
	for(int i=0; i<totalParams; i++)
		allControls[i] = NULL;

    delete frame;
    frame = NULL;

}

void EaselEditor::valueChanged(CControl* pControl)
{
	effect->setParameter(pControl->getTag(), pControl->getValue());
}

void EaselEditor::updateAllControls()
{
	if(frame)
	{
		for(int i=0; i<totalParams; i++)
		{
			if(allControls[i])
				allControls[i]->setValue(effect->getParameter(i));
		}

		frame->invalid();
	}
}