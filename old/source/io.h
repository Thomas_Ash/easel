//	io.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Input & Output modules
// - preamp & envelope detector
// - keyboard (MIDI is processed by Easel class)
// - output mixer
// - bridge: dummy module connecting the 3 patchpoints in 
//		the bottom left of the 208 to the internal buss

#ifndef __io__
#define __io__

#include "module.h"
#include "lowpassgate.h"	//for Vactrol

enum
{
	preampGainLow = 0,
	preampGainMid,
	preampGainHigh
};

enum
{
	shiftPreset = 0,
	shiftNone,
	shiftOctave
};

class PreampDet : public Module
{
	public:
		PreampDet(int inGain = preampGainLow);
		~PreampDet();

		void setGain(int inGain);

		void setBuffer(double* inBuffer, long inBufSize);	//provides pointer to incoming signal
						//size variable for safety; in case VST plugin class calls Module::tick() too many times
											
		Input* ampModIn;	//inputs

		Output* preampOut;	//outputs
		Output* envDetOut;

		void process();

	protected:
		int gain;		//vst params

		double* buffer;	//internal state
		long bufSize;
		Vactrol* opto;	



		double preampSig;	//output signals
		double envDetSig;

};


class Keyboard : public Module
{
	public:
		Keyboard(int inShift = shiftNone);
		~Keyboard();

		void setShift(int inShift);
		void setPresetLevel(double inPresetLevel, int index);

		Output* pulseOut;	//outputs
		Output* pressureOut;
		Output* keyOut;
		Output* presetOut;

		void setPressure(int inKey, double inPressure);	//called by MIDI event handler
		void setPreset(int inPreset);					//called by MIDI event handler

		void process();

		static const int numKeys = 29;

	protected:
		int shift;		//vst params
		double presetLevel[3];

		int key;		//internal state
		double pressure[numKeys];
		int preset;
		Vactrol* opto;

		double pulseSig;	//output signals
		double pressureSig;
		double keySig;
		double presetSig;
};

class OutputMix : public Module
{
	public:
		OutputMix(double inGainA = 1.0, double inGainB = 1.0, double inGainMaster = 0.5);
		~OutputMix();

		void setGainA(double inGainA);
		void setGainB(double inGainB);
		void setGainMaster(double inGainMaster);

		Input* audioAIn;	//inputs
		Input* audioBIn;

		Output* audioOut;	//output

		void process();

	protected:
		double gainA;		//vst params
		double gainB;
		double gainMaster;

		double audioSig;	//output signal
};

class Bridge : public Module
{
	public:
		Bridge();
		~Bridge();

		Input* pulseIn;		//inputs
		Input* pressureIn;
		Input* keyIn;

		Output* pulseOut;	//outputs
		Output* pressureOut;
		Output* keyOut;

		void process();

	protected:
		double pulseSig;	//output signals
		double pressureSig;
		double keySig;
};

#endif