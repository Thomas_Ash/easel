//	easel.h
//	Copyright Thomas Ash 2010

//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.

//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.

//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.


//Main class for VST Music Easel

#ifndef M_PI
#define M_PI 3.141592

#endif

#ifndef __easel__
#define __easel__

#include <audioeffectx.h>

#include "module.h"
#include "cv.h"
#include "lowpassgate.h"
#include "oscillators.h"
#include "io.h"
//#include "reverbmodel.h"

#define NUMPROGRAMS 16

enum
{
	paramRanTrigSrc = 0,
	paramSeqTrigSrc, paramSeqStages, paramSeqPul1, paramSeqPul2, paramSeqPul3, paramSeqPul4,
	paramSeqPul5, paramSeqLev1, paramSeqLev2, paramSeqLev3, paramSeqLev4, paramSeqLev5,
	paramEnvTrigSrc, paramEnvMode, paramEnvAttack, paramEnvDuration, paramEnvDecay,
	paramPulTrigSrc, paramPulMode, paramPulPeriod, paramPulPeriodProc,
	paramMOscDest, paramMOscKey, paramMOscFreq,	paramMOscFreqFine, paramMOscFreqProc, 
	paramMOscIndex, paramMOscIndexProc,	paramMOscWave,
	paramCOscKey, paramCOscPitch, paramCOscPitchFine, paramCOscPitchProc, paramCOscPitchProcSign,
    paramCOscTimb, paramCOscTimbProc, paramCOscShape, paramCOscWave,
    paramLpg1Offset, paramLpg1Proc, paramLpg1Mode,
	paramLpg2AudioSrc, paramLpg2Offset, paramLpg2Proc, paramLpg2Mode,
	paramPortTime, 
	paramKeyShift, paramKeyPreset1, paramKeyPreset2, paramKeyPreset3, 
	paramPreampGain,
	paramOutGainA, paramOutGainB, paramOutReverb, paramOutMonitor, paramOutMaster,

	inBPulse, inBPress, inBKey, inPulPeriod, inMOscIndex, inMOscFreq, inCOscPitch,
	inCOscTimb,	inLpg1Mod, inLpg2Mod, inInv, inKPorta,

    totalParams
};

enum
{
	outRan1 = 0, outRan2, outRan3, outRan4, outSeq1, outSeq2, outEnv1, outEnv2, outEnv3,
	outPul1, outPul2, outPul3, outPress1, outPress2, outPress3, outPress4, outInv,
	outMOsc, outEnvDet,	outKPulse1,	outKPulse2,	outKPress1,	outKPress2,	outKPorta1,
	outKPorta2,	outKKey1, outKKey2,	outKPreset1, outKPreset2,
	totalOuts
};

enum
{
	ledEnv = 0, ledPul, ledLpg1, ledLpg2, ledSeq1, ledSeq2, ledSeq3, ledSeq4, ledSeq5
};

class EaselProgram
{
	public:
		EaselProgram(int index = 0);
		~EaselProgram() {}

		float param[totalParams];
		char name[kVstMaxProgNameLen+1];
};

class Easel : public AudioEffectX
{
    public:
        Easel(audioMasterCallback audioMaster);
        ~Easel();

            // Processing
        virtual void processReplacing (float** inputs, float** outputs, VstInt32 sampleFrames);
        virtual void processDoubleReplacing (double** inputs, double** outputs, VstInt32 sampleFrames);

        // Parameters
        virtual void setParameter (VstInt32 index, float value);
        virtual float getParameter (VstInt32 index);
        virtual void getParameterLabel (VstInt32 index, char* label);
        virtual void getParameterDisplay (VstInt32 index, char* text);
        virtual void getParameterName (VstInt32 index, char* text);

		// Programs
		virtual void setProgram (VstInt32 index);
		virtual VstInt32 getProgram ();
		virtual void setProgramName (char* name);
		virtual void getProgramName (char* name);
		virtual bool getProgramNameIndexed (VstInt32 category, VstInt32 index, char* text);

		//MIDI stuff
		virtual VstInt32 canDo(char* text);
        virtual VstInt32 getNumMidiInputChannels ();
        virtual VstInt32 getNumMidiOutputChannels ();
        virtual VstInt32 processEvents (VstEvents* events);

		//other stuff
        virtual bool getEffectName (char* name);
        virtual bool getVendorString (char* text);
        virtual bool getProductString (char* text);
        virtual VstInt32 getVendorVersion ();

		//Extras
		Input* getInput(int index);
		Output* getOutput(int index);
		Output* getLedOutput(int index);

		void clickInput(int index);
		void doubleClickInput(int index);
		void clickOutput(int index);

    protected:
		float param[totalParams];

        EaselProgram* program[NUMPROGRAMS];
		int currentProg;

        Random* ran;			//modules
		Sequencer* seq;
        EnvGen* env;
		Pulser* pul;
		ModulationOsc* mOsc;
        ComplexOsc* cOsc;
        LowPassGate* lpg1;
        LowPassGate* lpg2;
		Inverter* inv;
		Portamento* port;
		Bridge* bri;
		Keyboard* key;
		PreampDet* pre;
		OutputMix* out;
	//	ReverbModel* rev;

		int holdingInput;
		int holdingOutput;

};




#endif // __easel__
